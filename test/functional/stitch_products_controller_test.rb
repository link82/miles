require 'test_helper'

class StitchProductsControllerTest < ActionController::TestCase
  setup do
    @stitch_product = stitch_products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:stitch_products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create stitch_product" do
    assert_difference('StitchProduct.count') do
      post :create, stitch_product: { brand: @stitch_product.brand, category_id: @stitch_product.category_id, description: @stitch_product.description, model: @stitch_product.model, product_id: @stitch_product.product_id, stitch_id: @stitch_product.stitch_id }
    end

    assert_redirected_to stitch_product_path(assigns(:stitch_product))
  end

  test "should show stitch_product" do
    get :show, id: @stitch_product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @stitch_product
    assert_response :success
  end

  test "should update stitch_product" do
    put :update, id: @stitch_product, stitch_product: { brand: @stitch_product.brand, category_id: @stitch_product.category_id, description: @stitch_product.description, model: @stitch_product.model, product_id: @stitch_product.product_id, stitch_id: @stitch_product.stitch_id }
    assert_redirected_to stitch_product_path(assigns(:stitch_product))
  end

  test "should destroy stitch_product" do
    assert_difference('StitchProduct.count', -1) do
      delete :destroy, id: @stitch_product
    end

    assert_redirected_to stitch_products_path
  end
end
