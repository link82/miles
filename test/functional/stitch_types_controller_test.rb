require 'test_helper'

class StitchTypesControllerTest < ActionController::TestCase
  setup do
    @stitch_type = stitch_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:stitch_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create stitch_type" do
    assert_difference('StitchType.count') do
      post :create, stitch_type: { description: @stitch_type.description, full_description: @stitch_type.full_description, leaf: @stitch_type.leaf, parent_id: @stitch_type.parent_id }
    end

    assert_redirected_to stitch_type_path(assigns(:stitch_type))
  end

  test "should show stitch_type" do
    get :show, id: @stitch_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @stitch_type
    assert_response :success
  end

  test "should update stitch_type" do
    put :update, id: @stitch_type, stitch_type: { description: @stitch_type.description, full_description: @stitch_type.full_description, leaf: @stitch_type.leaf, parent_id: @stitch_type.parent_id }
    assert_redirected_to stitch_type_path(assigns(:stitch_type))
  end

  test "should destroy stitch_type" do
    assert_difference('StitchType.count', -1) do
      delete :destroy, id: @stitch_type
    end

    assert_redirected_to stitch_types_path
  end
end
