require 'test_helper'

class MacroSeasonsControllerTest < ActionController::TestCase
  setup do
    @macro_season = macro_seasons(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:macro_seasons)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create macro_season" do
    assert_difference('MacroSeason.count') do
      post :create, macro_season: { code: @macro_season.code, description: @macro_season.description }
    end

    assert_redirected_to macro_season_path(assigns(:macro_season))
  end

  test "should show macro_season" do
    get :show, id: @macro_season
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @macro_season
    assert_response :success
  end

  test "should update macro_season" do
    put :update, id: @macro_season, macro_season: { code: @macro_season.code, description: @macro_season.description }
    assert_redirected_to macro_season_path(assigns(:macro_season))
  end

  test "should destroy macro_season" do
    assert_difference('MacroSeason.count', -1) do
      delete :destroy, id: @macro_season
    end

    assert_redirected_to macro_seasons_path
  end
end
