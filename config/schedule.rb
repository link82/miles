# Learn more: http://github.com/javan/whenever

#must match the application parameter used by capistrano
set :application, "miles"

every 1.hour, :roles => [:db], only: {primary: true} do
  #hourly redis backup (24 copies)
  command "backup perform -t #{application}_redis_hourly"
end

every 1.day, :at => '4:30 am', :roles => [:db], only: {primary: true} do
  #daily backup (7 copies)
  command "backup perform -t #{application}_db"
  command "backup perform -t #{application}_redis"
end