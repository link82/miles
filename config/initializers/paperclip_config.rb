if Rails.env == 'production'
	Paperclip.options[:command_path] = "/usr/bin/convert"
else
	Paperclip.options[:command_path] = "/usr/local/bin/convert"
end