require 'rabl'

Rabl.configure do |config|
  config.include_json_root = false
  config.include_child_root = false
  config.cache_all_output = false
  #config.cache_engine = Rabl::CacheEngine.new
  config.cache_sources = (Rails.env != 'development')
end
