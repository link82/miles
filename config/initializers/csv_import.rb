require 'csv'

def call_rake(task, options = {})
  options[:rails_env] ||= Rails.env
  args = options.map { |n, v| "#{n.to_s.upcase}='#{v}'" }
  system "/usr/bin/rake #{task} #{args.join(' ')} --trace 2>&1 >> #{Rails.root}/log/rake.log &"
end

def load_csv(file_name)

  if Rails.env != 'production'
    base_path = "#{Rails.root}/config/import/csv/"
  else
    base_path = "/media/milnas/export_csv/"
  end

  csv_text = File.read(base_path + file_name)
  #csv_text.force_encoding("iso-8859-1").encode("utf-8")



  begin
    lines = CSV.parse(csv_text, :headers => true)
  rescue StandardError

    begin
      lines = CSV.parse(csv_text.unpack("C*").pack("U*"), :headers => true)
    rescue StandardError
      puts "Import csv file #{file_name} is not encoded in UTF-8"
    end

  end
  #CSV.parse(csv_text)
  lines
end

def load_json(file_name)


  base_path = "#{Rails.root}/config/import/"

  json_text = File.read(base_path + file_name)


  begin
    hash = JSON.parse(json_text)
  rescue StandardError
    puts "Import json file #{file_name} is not encoded in UTF-8"
  end
  #CSV.parse(csv_text)
  hash
end

def import_i18n_for(model_name)

  model_name = model_name.to_s.to_sym
  return if [:stitch_type, :stitch].index(model_name).nil?

  file_names = {:stitch_type => "TipoPunto", :stitch => "Punto"}
  klass = model_name.to_s.camelize.constantize

  puts "Going to translate #{klass.class_name}"
  I18n.available_locales.each do |locale|
    file_name = file_names[model_name]
    Globalize.locale = locale.to_sym

    csv = load_csv("#{file_name}_#{locale.upcase}.CSV")
    puts "Translating to lang #{Globalize.locale}" if csv

    csv.each do |row|
      data = row.to_hash

      if data['code'].blank?
        puts "Skipped Stitch Type import, empty code found (possible wrong file encode?)"
        next
      end

      m = klass.with_code(data['code'])
      data.delete('id')
      data.delete('code')
      #bulk update values
      data.each do |key, val|
        #puts "Updating attribute #{key}:#{data[key]}"
        begin
          m.send("#{key}=",data[key])#
        rescue NoMethodError
          puts "Can't find field named #{key}"
        end
      end
      m.save!
    end unless csv.nil?
  end

end

def import_stitch_types(clean_data)

  clean_data = false if clean_data.nil?

  StitchType.delete_all if clean_data

  csv = load_csv("TipoPunto.CSV")
  if csv

    csv.each do |row|
      data = row.to_hash
      data['code'] = data['code'].to_s

      if data['code'].blank?
        puts "Skipped Stitch Type import, empty code found (possible wrong file encode?)"
        return
      end

      parent_id = nil
      unless data['parent_code'].to_i == 0
        parent = StitchType.with_code(data['parent_code'].to_s)
        unless parent.nil?
          parent_id = parent.id
          puts "--> Found parent for #{data['description']} with id #{parent_id}" unless (Rails.env == 'production')
        else
          puts "Missing parent for type with code #{data['code']}" unless (Rails.env == 'production')
        end
      end
      data.delete('id')
      data.delete('parent_code')

      is_leaf = data.delete('leaf').to_s == 'T'
      data['leaf'] = is_leaf
      data['parent_id'] = parent_id

      tmp = StitchType.find_or_initialize_by_code(data['code'])
      tmp.update_attributes! data

      puts "Created Stitch type '#{data['description']}'" unless (Rails.env == 'production')

    end

  end

end


def import_suppliers(clean_data)

  clean_data = false if clean_data.nil?


  Supplier.delete_all if clean_data

  csv = load_csv("Fornitore.CSV")
  if csv

    csv.each do |row|
      next if row.blank?

      data = row.to_hash
      data['code'] = data['code'].to_s

      if data['code'].blank?
        puts "Skipped Supplier import, empty code found (possible wrong file encode?)"
        return
      end


      tmp = Supplier.find_or_initialize_by_code(data['code'])
      tmp.update_attributes! data

      puts "Created Supplier '#{data['description']}'" unless (Rails.env == 'production')

    end

  end

end

def import_yarns(clean_data)

  clean_data = false if clean_data.nil?

  Yarn.delete_all if clean_data

  csv = load_csv("Filato.CSV")
  if csv

    csv.each do |row|
      data = row.to_hash
      data['code'] = data['code'].to_s

      if data['code'].blank?
        puts "Skipped Yarn import, empty code found (possible wrong file encode?)"
        return
      end

      tmp = Yarn.find_or_initialize_by_code(data['code'])
      tmp.update_attributes! data

      puts "Created Yarn '#{data['description']}'" unless (Rails.env == 'production')

    end

  end

end

def import_machines(clean_data)

  clean_data = false if clean_data.nil?

  Machine.delete_all if clean_data

  csv = load_csv("Macchina.CSV")
  if csv

    csv.each do |row|
      data = row.to_hash
      data['code'] = data['code'].to_s

      if data['code'].blank?
        puts "Skipped Machine import, empty code found (possible wrong file encode?)"
        return
      end


      tmp = Machine.find_or_initialize_by_code(data['code'])
      tmp.update_attributes! data

      puts "Created Machine '#{data['description']}'" unless (Rails.env == 'production')

    end

  end

end

def import_products(clean_data)

  clean_data = false if clean_data.nil?

  Product.delete_all if clean_data

  csv = load_csv("Prodotto.CSV")
  if csv

    csv.each do |row|
      data = row.to_hash
      data['code'] = data['code'].to_s

      if data['code'].blank?
        puts "Skipped Product import, empty code found (possible wrong file encode?)"
        return
      end


      tmp = Product.find_or_initialize_by_code(data['code'])
      tmp.update_attributes! data

      puts "Created Product '#{data['description']}'" unless (Rails.env == 'production')

    end

  end

end

def import_seasons(clean_data)

  clean_data = false if clean_data.nil?

  MacroSeason.delete_all if clean_data

  csv = load_csv("Macrostagione.CSV")
  if csv

    csv.each do |row|
      data = row.to_hash
      data['code'] = data['code'].to_s

      if data['code'].blank?
        puts "Skipped MacroSeason import, empty code found (possible wrong file encode?)"
        return
      end

      tmp = MacroSeason.find_or_initialize_by_code(data['code'])
      tmp.update_attributes! data

      puts "Created Macro Season '#{data['description']}'" unless (Rails.env == 'production')

    end

  end

end

def import_brands(clean_data)

  clean_data = false if clean_data.nil?


  Brand.delete_all if clean_data

  csv = load_csv("Marchio.CSV")
  if csv

    csv.each do |row|
      data = row.to_hash
      data['code'] = data['code'].to_s

      if data['code'].blank?
        puts "Skipped Brand import, empty code found (possible wrong file encode?)"
        return
      end

      tmp = Brand.find_or_initialize_by_code(data['code'])
      tmp.update_attributes! data

      puts "Created Brand #{data['code'].to_s} - '#{data['description']}'" unless (Rails.env == 'production')

    end

  end

end

def import_pictures(clean_data)
  #qui ha poco senso
  clean_data = false if clean_data.nil?

  # Used to import pictures record matching images archive, il will be called before importing stitches
  # while importing stitches pictures will be re-connected to relative stitch

  #Picture.find_each{|p| p.destroy} if clean_data

  #I proceed if I don't have any picture record
  #even if I did not erase any picture

  if(Picture.count > 0)
    puts "----- Skipping picture records import, records already present -----"
    return
  else
    puts "----- No record found for pictures, importing records -----"
  end

  json = load_json("pictures.json")
  if json

    json.each do |data|


      puts "------------------------------"
      p = Picture.new

      data.keys.each do |k|
        puts "Assigned #{k}: #{data[k]}"
        p[k] = data[k]
      end
      puts "------------------------------"
      p.save!

      puts "Created Picture '#{data['description']}'" unless (Rails.env == 'production')

    end

    puts "Data import completed, updating autoincrement value"
    table = 'pictures'
    auto_inc_val = Picture.maximum(:id) +1   # New auto increment start point
    ActiveRecord::Base.connection.execute(
      "ALTER SEQUENCE #{table}_id_seq RESTART WITH #{auto_inc_val}"
    )

  end

end


def import_stitch_type_products(force_import,generate_thumbs,clean_data)

  clean_data = false if clean_data.nil?

  puts "Importing stitch types products"

  StitchTypeProduct.delete_all if clean_data

  csv = load_csv("TipoPuntoProdotto.CSV")
  if csv

    csv.each do |row|
      data = row.to_hash
      data['leaf'] = data['leaf'].to_s

      if data['leaf'].blank?
        puts "Skipped Stitch import, empty code found (possible wrong file encode?)"
        return
      end

      stitch_type_code = data.delete('leaf')
      data['stitch_type_code'] = stitch_type_code
      stitch_type = StitchType.with_code(stitch_type_code) unless stitch_type_code.blank?
      data['stitch_type_id'] = stitch_type.id unless stitch_type.nil?

      product = Product.with_code(data['product_code'])
      data['product_id'] = product.id unless product.nil?

      data['updated_picture'] = (force_import ? true : data['updated_picture'])
      data['updated_sketch'] = (force_import ? true : data['updated_sketch'])

      s = StitchTypeProduct.find_or_initialize_by_stitch_type_and_product_code(data['stitch_type_code'],data['product_code'])
      s.update_attributes! data
      puts "Created stitch type code #{s.stitch_type_code} - #{s.description}" unless (Rails.env == 'production')


      if(generate_thumbs || force_import || Rails.env == 'production')
        s.delay.import_images
      end
    end

  end

end



def import_stitches(force_import,generate_thumbs,clean_data,fg_processing)

  clean_data = false if clean_data.nil?

  if Rails.env != 'production'
    image_path = "#{Rails.root}/config/import/immagini"
  else
    image_path = "/media/milnas/immagini_punti"
  end
  #CSV
  #code,year,season_code,brand_code,composition,yarn_code1,yarn_code2,yarn_code3,yarn_code4,
  #product_code1,product_code2,product_code3,product_code4,machine_code,thinness,stretch,vanise,
  #stitch_type_code1,stitch_type_code2,stitch_type_code3,leaf_code,description,creation_time

  #MODEL
  #attr_accessible :brand_id, :composition, :description, :machine_id, :season_code, :stretch, :thinness, :vanise, :year, :code

  Stitch.delete_all if clean_data
  StitchProduct.delete_all #if clean_data
  StitchYarn.delete_all #if clean_data

  csv = load_csv("Punto.CSV")
  if csv

    csv.each do |row|
      data = row.to_hash
      data['code'] = data['code'].to_s

      if data['code'].blank?
        puts "Skipped Stitch import, empty code found (possible wrong file encode?)"
        return
      end

      yarns = []
      yarn = data.delete('yarn_code1')
      yarns << Yarn.with_code(yarn.to_s) unless yarn.blank?
      yarn = data.delete('yarn_code2')
      yarns << Yarn.with_code(yarn.to_s) unless yarn.blank?
      yarn = data.delete('yarn_code3')
      yarns << Yarn.with_code(yarn.to_s) unless yarn.blank?
      yarn = data.delete('yarn_code4')
      yarns << Yarn.with_code(yarn.to_s) unless yarn.blank?
      yarns.compact!

      #Extract suppliers from yarns
      suppliers = []
      yarns.each do |y|
        suppliers << y.supplier unless y.supplier.blank?
      end
      suppliers.compact!


      products = []
      product = data.delete('product_code1')
      products << Product.with_code(product.to_s) unless product.blank?
      product = data.delete('product_code2')
      products << Product.with_code(product.to_s) unless product.blank?
      product = data.delete('product_code3')
      products << Product.with_code(product.to_s) unless product.blank?
      product = data.delete('product_code4')
      products << Product.with_code(product.to_s) unless product.blank?
      products.compact!

      types = []
      type = data.delete('stitch_type_code1')
      types << StitchType.with_code(type.to_s) unless type.blank?
      type = data.delete('stitch_type_code2')
      types << StitchType.with_code(type.to_s) unless type.blank?
      type = data.delete('stitch_type_code3')
      types << StitchType.with_code(type.to_s) unless type.blank?
      types.compact!

      data['yarn_codes'] = "#" + yarns.compact.collect{|y| y.id}.join('#') + "#"
      data['supplier_codes'] = "#" + suppliers.compact.collect{|s| s.id}.join('#') + "#"
      data['product_codes'] = "#" + products.collect{|p| p.id}.join('#') + "#"
      data['stitch_type_codes'] = "#" + types.collect{|t| t.id}.join('#') + "#"
      data['stitch_type_id'] = types.last.id unless types.empty? #need to get the leaf

      brand_code = data.delete('brand_code')
      brand = Brand.with_code(brand_code) unless brand_code.blank?
      puts "Found brand code: #{brand_code}"

      data['brand_id'] = brand.id unless brand.nil?
      machine = Machine.with_code(data.delete('machine_code'))
      data['machine_id'] = machine.id unless machine.nil?


      data.delete('leaf')
      data.delete('creation_time')
      data['stretch'] = (data.delete('stretch') == 'T')
      data['vanise'] = (data.delete('vanise') == 'T')

      #Check if need to import images
      data['updated_images'] = (data['updated_images'].to_s == '1' || force_import) ? true : false

      s = Stitch.find_or_initialize_by_code(data['code'])
      s.update_attributes! data
      puts "Created stitch #{s.code} - #{s.description}" unless (Rails.env == 'production')
      s.save
      s.reload

      #DA RIVEDERE PER NON CREARE RECORD OGNI VOLTA
      #ATTENDI NUOVA LOGICA PER PRODUCTS
      products.each do |p|
        s.stitch_products.create!(:product_id => p.id)
        #puts "Connected Stitch to Product #{p.description}"
      end

      yarns.each do |y|
        s.stitch_yarns.create!(:yarn_id => y.id)
        #puts "Connected Stitch to Yarn #{y.description}"
      end

      # if s.updated_images
      #   puts "Updated image"
      #   if generate_thumbs
      #     puts "-- Re/Importing images for stitch #{s.code} #{fg_processing ? 'in foreground' : 'in background'}"
      #     fg_processing ? Picture.import_from_stitch(s,data['code'] ,image_path) : Picture.delay.import(data['code'] ,image_path)
      #   else
      #     #puts "-- Skipping thumb generation, trying to reconnect pictures with current stitch"
      #     Picture.reconnect(data['code'])
      #   end
      # else #doesnt remove pictures, just reconnect with new stitch_id for that code
      #   #puts "-- Image NOT updated, reconnecting pictures"
      #   Picture.reconnect(data['code'])
      # end

    end

  end

end
