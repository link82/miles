set :bundle_flags,    "--deployment" #--quiet

require "bundler/capistrano"
require "delayed/recipes"
require 'new_relic/recipes'
#require 'sidekiq/capistrano'

load "config/recipes/base"
load "config/recipes/nginx"
load "config/recipes/unicorn"
load "config/recipes/postgresql"
#load "config/recipes/backup"
#load "config/recipes/nodejs"
#load "config/recipes/rbenv"
#load "config/recipes/check"

#load "config/recipes/monit"
load "config/recipes/redis"
load "config/recipes/import"


#load "config/recipes/memcached"

# MANAGE CONFIG FILES FROM SHARED FOLDER
load "config/recipes/config_files"
set :shared_files, %w(config/database.yml config/newrelic.yml config/redis.yml config/s3.yml)

set :nginx_port, 5000
#set :stages, %w[production]
#set :default_stage, 'production'

# Does not autostart unicorn if monit is installed
set :monit_installed, true


#192.168.1.225
#80.22.197.83
#Stitch.updated(true).each{|s| Picture.delay.import(s.code.to_s,'/media/milnas/immagini_punti')}
#marchio:
# VA  4366
# SF  595
set :domain, "80.22.197.83"
set :server_url_name, "miles.bluetouchdev.com"

#CUSTOM KEYS FOR EXTERNAL SERVICES
set :newrelic_license_key, 'cf53ea7194aca62b1df037e77ac784bbf466d3ed'


role :app, domain
role :web, domain
role :db,  domain, :primary => true



## USED BY MONIT TO SEND NOTIFICATION EMAILS
set :mailer_user, "info@bluetouchdev.com"
set :mailer_pass, "BlueTouch2011"

set :admin_email, "info@bluetouchdev.com"
###################################
# Monit web interface credentials
set :monit_user, "admin"
set :monit_pass, "BlueTouch2011"
###################################


set :application, "miles"

#NEEDE TO MATCH HIS DIFFERENT NAME
set :database_name, "miles_production" # -> used in /config/recipes/postgresql.rb

set :user, "deployer"
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, "git"
set :repository,  "https://link82@bitbucket.org/link82/miles.git"
set :branch, "master"

set :maintenance_template_path, File.expand_path("recipes/templates/maintenance.html.erb", __FILE__)


set :delayed_job_args, "-n 6" #instantiate 4 workers

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
#ssh_options[:verbose] = :info

after "deploy", "deploy:cleanup" # keep only the last 5 releases
after "deploy:update", "newrelic:notice_deployment"

# NECESSITA REVISIONE PER ALCUNE DIPENDENZE
# To setup new Ubuntu 12.04 server:
# ssh root@69.164.192.207
# adduser deployer
# echo "deployer ALL=(ALL:ALL) ALL" >> /etc/sudoers
# exit
# ssh-copy-id deployer@69.164.192.207
# cap deploy:install
# cap deploy:setup
# cap deploy:cold




