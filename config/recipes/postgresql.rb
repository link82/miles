set_default(:postgresql_host, "localhost")
set_default(:postgresql_user) { user }
set_default(:postgresql_password) { Capistrano::CLI.password_prompt "PostgreSQL Password: " }
set_default(:postgresql_database) { "#{database_name}" }
set_default(:postgresql_pid) { "/var/run/postgresql/9.1-main.pid" }
set_default :postgresql_pool, 25

namespace :postgresql do

  desc "Install the latest stable release of PostgreSQL."
  task :install, roles: :db, only: {primary: true} do
    #run "#{sudo} add-apt-repository ppa:pitti/postgresql"
    #run "#{sudo} apt-get -y update"
    #run "#{sudo} apt-get -y install postgresql libpq-dev"
  end
  after "deploy:install", "postgresql:install"

  desc "Create a database for this application."
  task :create_database, roles: :db, only: {primary: true} do
    run %Q{#{sudo} -u postgres psql -c "create user #{postgresql_user} with password '#{postgresql_password}';"}
    run %Q{#{sudo} -u postgres psql -c "create database #{postgresql_database} owner #{postgresql_user};"}
  end
  after "postgresql:setup", "postgresql:create_database"

  desc "Removing application database"
  task :remove, roles: :db, on_error: :continue do
    run %Q{#{sudo} -u postgres psql -c "drop database #{postgresql_database};"}
    run %Q{#{sudo} -u postgres psql -c "drop user #{postgresql_user};"}
  end
  before "deploy:remove", "postgresql:remove"

  desc "Generate the database.yml configuration file."
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    template "postgresql.yml.erb", "#{shared_path}/config/database.yml"
  end
  after "deploy:setup", "postgresql:setup"

  desc "Symlink the database.yml file into latest release"
  task :symlink, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
  after "deploy:finalize_update", "postgresql:symlink"

  desc "Run a task on a remote server."
  # run like: cap staging rake:invoke task=a_certain_task  
  task :reset do
    run("cd #{deploy_to}/current && bundle exec rake db:drop RAILS_ENV=production")
    run %Q{#{sudo} -u postgres psql -c "create database #{postgresql_database} owner #{postgresql_user};"}
    run("cd #{deploy_to}/current && bundle exec rake db:migrate RAILS_ENV=production")
  end

  desc "Seeding database"
  task :seed, on_error: :continue do
    monit.stop
    delayed_job.stop
    unicorn.stop
    puts "Going to sleep 15 sec until unicorn stops"
    sleep 15
    postgresql.reset
    unicorn.start
    puts "Going to sleep 10 sec until unicorn starts"
    sleep 15
    delayed_job.start
    monit.start
    run("cd #{deploy_to}/current && bundle exec rake db:seed kind=all import_images=true RAILS_ENV=production")

  end

end



  