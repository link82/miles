namespace :nginx do
  desc "Install latest stable release of nginx"
  task :install, roles: :web do
    #run "#{sudo} add-apt-repository ppa:nginx/stable"
    #run "#{sudo} apt-get -y update"
    #run "#{sudo} apt-get -y install nginx"
  end
  after "deploy:install", "nginx:install"

  desc "Setup nginx configuration for this application"
  task :setup, roles: :web do
    template "nginx_unicorn.erb", "/tmp/nginx_conf"
    run "#{sudo} mv /tmp/nginx_conf /etc/nginx/sites-enabled/#{application}"
    run "#{sudo} rm -f /etc/nginx/sites-enabled/default"
    restart
  end
  after "deploy:setup", "nginx:setup"

  desc "Removing Nginx application config file and restarting server"
  task :remove, roles: :web do
    run "#{sudo} service nginx stop"
    run "#{sudo} rm -f /etc/nginx/sites-enabled/#{application}"
    run "#{sudo} ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/"
    run "#{sudo} service nginx start"
  end
  after "deploy:remove", "nginx:remove"
  
  %w[start stop restart].each do |command|
    desc "#{command} nginx"
    task command, roles: :web do
      run "#{sudo} service nginx #{command}"
    end
  end
end
