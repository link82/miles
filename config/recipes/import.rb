set_default(:base_path, "./config/import/csv")

namespace :import do

  desc "Copying import files to destination server"
  task :put_csv, roles: :app do

    set :nome_file, Proc.new { deploy_location = Capistrano::CLI.ui.ask "Enter name of the file to download (leave empty to download all files)" }

    if(nome_file != "")
      puts "Uploading file #{base_path}/#{nome_file} -> /media/milnas/export_csv/#{nome_file}"
      upload "#{base_path}/#{nome_file}" , "/media/milnas/export_csv/#{nome_file}"
    else

      [
        "TipoPunto.CSV","TipoPunto_IT.CSV","TipoPunto_EN.CSV","TipoPunto_FR.CSV",
        "Fornitore.CSV",
        "Filato.CSV",
        "Macchina.CSV",
        "Prodotto.CSV",
        "Macrostagione.CSV",
        "Punto.CSV", "Punto_IT.CSV", "Punto_EN.CSV", "Punto_FR.CSV",
        "Marchio.CSV",
        "TipoPuntoProdotto.CSV"
      ].each do |shared_file|
    		puts "Uploading file #{base_path}/#{shared_file} -> /media/milnas/export_csv/#{shared_file}"
    		upload "#{base_path}/#{shared_file}" , "/media/milnas/export_csv/#{shared_file}"
    	end

    end
  end

  desc "Copying import files to destination server"
  task :get_csv, roles: :app do

    set :nome_file, Proc.new { deploy_location = Capistrano::CLI.ui.ask "Enter name of the file to download (leave empty to download all files)" }

    if(nome_file != "")
      puts "Downloading file #{base_path}/#{nome_file} from /media/milnas/export_csv/#{nome_file}"
      download "/media/milnas/export_csv/#{nome_file}", "#{base_path}/#{nome_file}"
    else
      [
        "TipoPunto.CSV","TipoPunto_IT.CSV","TipoPunto_EN.CSV","TipoPunto_FR.CSV",
        "Fornitore.CSV",
        "Filato.CSV",
        "Macchina.CSV",
        "Prodotto.CSV",
        "Macrostagione.CSV",
        "Punto.CSV", "Punto_IT.CSV", "Punto_EN.CSV", "Punto_FR.CSV",
        "Marchio.CSV",
        "TipoPuntoProdotto.CSV"
      ].each do |shared_file|
        puts "Downloading file #{base_path}/#{shared_file} from /media/milnas/export_csv/#{shared_file}"
        download "/media/milnas/export_csv/#{shared_file}", "#{base_path}/#{shared_file}"
      end

    end
  end

  ## Stitch
  desc "Copying stitches image archive locally"
  task :get_stitches_archive, roles: :app do
  	download "#{current_path}/public/system/images.zip", "./public/system/images_bck.zip"
  end

  desc "Copying stitches image archive locally"
  task :get_updated_stitches_archive, roles: :app do
    download "#{current_path}/public/system/archives/updated_stitches_images.zip", "./public/system/updated_stitches_images.zip"
  end
  ## Stitch


  ## Stitch Type product
  desc "Copying products image archive/s locally"
  task :get_products_archives, roles: :app do

    (1..100).each do |nr|
      puts "Downloading archive #{current_path}/public/system/archives/products_images_#{nr}.zip..."
      download "#{current_path}/public/system/archives/products_images_#{nr}.zip", "./public/system/products_images_#{nr}.zip"
    end
  end

  ## Stitch Type product
  desc "Copying products sketches archive/s locally"
  task :get_products_sketches_archives, roles: :app do

    (1..100).each do |nr|
      puts "Downloading archive #{current_path}/public/system/archives/products_sketches_#{nr}.zip..."
      download "#{current_path}/public/system/archives/products_sketches_#{nr}.zip", "./public/system/products_sketches_#{nr}.zip"
    end
  end

  desc "Copying stitches image archive locally"
  task :get_updated_products_images_archives, roles: :app do
    download "#{current_path}/public/system/archives/updated_products_images.zip", "./public/system/updated_products_images.zip"
  end

  desc "Copying stitches sketches archive locally"
  task :get_updated_products_sketches_archives, roles: :app do
    download "#{current_path}/public/system/archives/updated_products_sketches.zip", "./public/system/updated_products_sketches.zip"
  end
  ## Stitch Type product





  # desc "Copying products image archive locally"
  # task :get_products_picture_archive, roles: :app do
  #   download "/home/deployer/product_images.tar.gz", "./config/import/product_images_bck.tar.gz"
  # end

  # desc "Copying products sketch archive locally"
  # task :get_products_sketch_archive, roles: :app do
  #   download "/home/deployer/product_sketches.tar.gz", "./config/import/product_sketches.tar.gz"
  # end

  desc "Open the rails console on one of the remote servers"
  task :empty_catalog, :roles => :app do
    hostname = find_servers_for_task(current_task).first
    port = exists?(:port) ? fetch(:port) : 22
    exec "ssh -l #{user} #{hostname} -p #{port} -t 'source ~/.profile && cd #{current_path} && RAILS_ENV=production rake db:drop && RAILS_ENV=production rake db:create && RAILS_ENV=production rake db:migrate'"
  end

  desc "Open the rails console on one of the remote servers"
  task :update_catalog, :roles => :app do
    hostname = find_servers_for_task(current_task).first
    port = exists?(:port) ? fetch(:port) : 22
    exec "ssh -l #{user} #{hostname} -p #{port} -t 'source ~/.profile && cd #{current_path} && RAILS_ENV=production rake db:seed kind=all generate_thumbs=true'"
  end


end


