
namespace :config_files do

  desc "Generating configuration files for external services"

  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    #template "postgresql.yml.erb", "#{shared_path}/config/database.yml"
    # aa
    #template "parse.yml.erb", "#{shared_path}/config/parse_resource.yml"
    #template "s3.yml.erb", "#{shared_path}/config/s3.yml"
    template "newrelic.yml.erb", "#{shared_path}/config/newrelic.yml"
    #template "memcached.yml.erb", "#{shared_path}/config/memcached.yml"
  end
  after "deploy:setup", "config_files:setup"

  desc "Creating symlinks to config files for current release"
  task :symlinks, roles: :app, on_error: :continue do
  	shared_files.each do |shared_file|
  		puts "Replacing file #{shared_file} -> #{shared_path}/#{shared_file}"
  		run "ln -nfs #{shared_path}/#{shared_file} #{release_path}/#{shared_file}"
  	end
  end
  before "deploy:migrate", "config_files:symlinks"

end
