set_default(:redis_host, "127.0.0.1")
set_default(:redis_port) { 6379 }

namespace :redis do

  desc "Install redis"
  task :install do
    run "#{sudo} apt-get update"
    run "#{sudo} apt-get -y install redis-server"
    run "#{sudo} cp /etc/redis/redis.conf /etc/redis/redis.conf.default"
    run "#{sudo} usermod -G redis -a '#{user}'"
  end
  
  after "deploy:install", "redis:install"

  desc "Generate the redis.yml configuration file."
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    template "redis.yml.erb", "#{shared_path}/config/redis.yml"
  end
  after "deploy:setup", "redis:setup"

  desc "Copying locally Redis dump file"
  task :get, roles: :app do
    run "rm -f ./redis-server/dump.rdb"
    download "/var/lib/redis/dump.rdb", "./redis-server/dump.rdb"
  end

  desc "Putting local copy of Redis dump to server"
  task :put, roles: :app do
    stop
    upload "./redis-server/dump.rdb","/var/lib/redis/dump.rdb"
    start
  end


  %w[start stop restart].each do |command|
    desc "#{command} redis"
    task command, roles: :web do
      run "#{sudo} service redis-server #{command}"
    end
  end
  after "redis:setup", "redis:start"

end