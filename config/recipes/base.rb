def template(from, to)
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  put ERB.new(erb).result(binding), to
end

def set_default(name, *args, &block)
  set(name, *args, &block) unless exists?(name)
end

namespace :rails do
  
  desc "Open the rails console on one of the remote servers"
  task :console, :roles => :app do
    hostname = find_servers_for_task(current_task).first
    port = exists?(:port) ? fetch(:port) : 22
    exec "ssh -l #{user} #{hostname} -p #{port} -t 'source ~/.profile && #{current_path}/script/rails c #{rails_env}'"
  end

  desc "Attaching to production log file"
  task :log, :roles => :app do
    hostname = find_servers_for_task(current_task).first
    port = exists?(:port) ? fetch(:port) : 22
    exec "ssh -l #{user} #{hostname} -p #{port} -t 'source ~/.profile && cd #{current_path}/log && tail -f production.log'"
  end

  desc "Open taps server"
  task :taps_server, :roles => :app do
    hostname = find_servers_for_task(current_task).first
    port = exists?(:port) ? fetch(:port) : 22
    exec "ssh -l #{user} #{hostname} -p #{port} -t 'source ~/.profile && #{current_path}/server_taps.sh'"
  end 

end


namespace :deploy do
  desc "Install everything onto the server"
  
  task :install do
    run "#{sudo} apt-get -y update"
    run "#{sudo} apt-get -y install python-software-properties"
  end

  desc "Removing application files"
  task :remove do
    run "cd ~/apps"
    run "#{sudo} rm -rf apps/#{application}"
  end

  desc "Search Workers processes"
  task :workers, roles: :app do
    set :worker_name, Proc.new { worker_name = Capistrano::CLI.ui.ask "Please specify the worker name to look for" }
    run "ps aux | grep #{worker_name}"
  end

  desc "Search for processes"
  task :processes, roles: :app do
    set :proc_name, Proc.new { proc_name = Capistrano::CLI.ui.ask "Please specify the process name to look for" }
    run "ps -A | grep #{proc_name}"
  end

  task :seed_db, roles: :db do

    set :reset_db, Proc.new { reset_db = Capistrano::CLI.ui.ask "Would you like to rebuild database before? (Y|N)" }
    set :kind, Proc.new { kind = Capistrano::CLI.ui.ask "Enter kind parameter for seeding db" }
    set :import, Proc.new { import = Capistrano::CLI.ui.ask "Would you like to import images? (Y|N)" }

    if reset_db == 'Y'
      puts "Droppping database..."
      run "cd #{current_path}; rake db:drop RAILS_ENV=#{rails_env}"

      puts "Creating database..."
      run "cd #{current_path}; rake db:create RAILS_ENV=#{rails_env}"

      puts "Migrating database..."
      run "cd #{current_path}; rake db:migrate RAILS_ENV=#{rails_env}"

      puts "== Database recreated!! =="
    end

    if import == 'Y'
      run "cd #{current_path}; rake db:seed kind=#{kind} import_images=true RAILS_ENV=#{rails_env}"
    else
      run "cd #{current_path}; rake db:seed kind=#{kind} RAILS_ENV=#{rails_env}"
    end
    
    puts "Database seeded!" 
  end

  #after :bundle_install, "deploy:migrate"
  before "deploy:assets:precompile", "deploy:migrate"

  ## CAPISTRANO STUFF ##
  after "deploy:stop",    "delayed_job:stop"
  after "deploy:start",   "delayed_job:start"
  after "deploy:restart", "delayed_job:restart"
end
