set_default(:backup_db_host, "localhost")
set_default(:backup_db_user) { "" }
set_default(:backup_db_password) { "" }
set_default(:backup_db_name) { "#{database_name}" }

set_default(:s3_access_key) { "" }
set_default(:s3_secret_key) { "" }
set_default(:s3_region) { "us-east-1" }
set_default(:s3_bucket) { "bluetouchdev-backups" }
set_default(:s3_path) { "" } #don't use / in path, there seems to be a bug
set_default(:max_backups){ 7 }


set :email_from, "dagon@needle.me"
## DEFINED IN DEPLOY.RB USED TO SEND NOTIFICATION EMAILS
#set :mailer_user, "davide.cenzi@needle.me"
#set :mailer_pass, "BlueTouch2011"

#set :admin_email, "davide.cenzi@needle.me"
###################################

set :backup_hour, 5
set :backup_minutes, 0

namespace :backup do

  desc "Install the required gem and create directory structure for backups"
  task :install, roles: :db, only: {primary: true} do
    run "cd ~"
    run "gem install backup --no-ri --no-rdoc"
    run "mkdir -p ~/Backup/models Backup/log ~/Backup/data"
  end

  desc "Setup automatic database backups"
  task :setup, roles: :db, only: {primary: true} do
    
    #backup model file
    template "backup/database.erb", "/tmp/backup"
    run "mv -f /tmp/backup ~/Backup/models/#{application}_db.rb"

    template "backup/redis.erb", "/tmp/backup"
    run "mv -f /tmp/backup ~/Backup/models/#{application}_redis.rb"

    template "backup/redis_hourly.erb", "/tmp/backup"
    run "mv -f /tmp/backup ~/Backup/models/#{application}_redis_hourly.rb"

    #generic backup config file
    template "backup/config.erb", "/tmp/config_backup"
    run "mv -f /tmp/config_backup ~/Backup/config.rb"

  end
  before "whenever:update_crontab", "backup:setup"
  #REMEMBER TO DISABLE WHENEVER WHEN YOU DON'T NEED BACKUPS

  desc "Perform backup now"
  task :perform, roles: :db, only: {primary: true} do

    set_default(:backup_task) { Capistrano::CLI.ui.ask "Enter backup task name (skip app name #{application}_) i.e db,redis,redis_hourly : " }

    run "cd ~"
    run "backup perform -t #{application}_#{backup_task}"
  end

end

