set_default(:memcached_host, "127.0.0.1")
set_default(:memcached_port, "11211")
set_default :memcached_memory_limit, 64


namespace :memcached do

  desc "Install Memcached"
  task :install, roles: :app do
    run "#{sudo} apt-get -y install memcached"
  end
  after "deploy:install", "memcached:install"

  desc "Setup Memcached"
  task :setup, roles: :app do
    template "memcached.erb", "/tmp/memcached.conf"

    run "#{sudo} mv /tmp/memcached.conf /etc/memcached_#{application}.conf"
    template "memcached.yml.erb", "#{shared_path}/config/#{application}.yml"

    #run "#{sudo} /bin/kill -9 `cat /var/run/memcached/memcached.pid`; rm /var/run/memcached/memcached.pid"
    stop
    start
  end
  after "deploy:setup", "memcached:setup"

  desc "Clear Memcached data"
  task :flush, roles: :app do
    run "echo 'flush_all' | nc #{memcached_host} #{memcached_port}"
  end

  desc "Getting memcached stats"
  task :stats, roles: :app do
    run "echo 'stats settings' | nc #{memcached_host} #{memcached_port}"
  end

  %w[start stop restart].each do |command|
    desc "#{command} Memcached"
    task command, roles: :app do
      run "#{sudo} /etc/init.d/memcached #{command}"
    end
  end

end
