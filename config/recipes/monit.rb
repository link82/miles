namespace :monit do
  desc "Install Monit"
  task :install do
    run "#{sudo} apt-get -y install monit"
  end
  before "deploy:install", "monit:install"

  desc "Setup all Monit configuration"
  task :setup do
    monit_config "monitrc", "/etc/monit/monitrc"
    nginx
    postgresql
    unicorn
    redis
    #memcached
    syntax
    reload
    #/usr/bin/monit -c /etc/monit/monitrc
    run "#{sudo} update-rc.d -f monit defaults"
  end
  after "deploy:setup", "monit:setup"

  desc "Removing monit configuration files and stopping service"
  task :remove do
    run "#{sudo} update-rc.d -f monit remove"
    run "#{sudo} rm -f /etc/monit/conf.d/*.conf"
  end
  before "monit:remove", "monit:stop"
  before "deploy:remove", "monit:remove"

  task(:nginx, roles: :web) { monit_config "nginx" }
  task(:postgresql, roles: :db) { monit_config "postgresql" }
  task(:redis, roles: :db) { monit_config "redis" }
  #task(:memcached, roles: :db) { monit_config "memcached" }
  task(:unicorn, roles: :app) { monit_config "unicorn" }

  %w[start stop restart syntax reload].each do |command|
    desc "Run Monit #{command} script"
    task command do
      run "#{sudo} service monit #{command}"
    end
  end
  #Automatically shut down monit during deploy
  before "deploy", "monit:stop"
  before "deploy:migrations", "monit:stop"

  #Automatically reload monit after deploy
  after "deploy", "monit:start"
  after "deploy:migrations", "monit:start"

end

def monit_config(name, destination = nil)
  destination ||= "/etc/monit/conf.d/#{name}.conf"
  template "monit/#{name}.erb", "/tmp/monit_#{name}"
  run "#{sudo} mv /tmp/monit_#{name} #{destination}"
  run "#{sudo} chown root #{destination}"
  run "#{sudo} chmod 600 #{destination}"
end
