#require 'sidekiq/web'

Miles::Application.routes.draw do

  #mount Sidekiq::Web, at: '/sidekiq', as: :sidekiq
  match "/queue" => DelayedJobWeb, :anchor => false

  scope '/:locale', locale: /#{I18n.available_locales.join('|')}/i do

    get 'archives_count', as: :archive_count, :to => "stitches#archives_count"

    resources :brands do
      collection do
        get 'count'
      end
    end


    resources :stitch_type_products do
      collection do
        get 'count'
      end
    end

    resources :suppliers do
      collection do
        get 'count'
      end
    end



    resources :macro_seasons do
      collection do
        get 'count'
      end
    end

    resources :stitches do
      collection do
        get 'archive', as: :archive
        get 'count'
      end


      resources :pictures, :only => [:index, :show]

    end

    resources :pictures do
      collection do
        get 'download_url', :as => "download_archive"
        get 'export'
        get 'count'
      end
    end


    resources :stitch_products do
      collection do
        get 'count'
      end
    end

    resources :yarns do
      collection do
        get 'count'
      end
    end

    resources :products do
      collection do
        get 'count'
      end
    end

    resources :machines do
      collection do
        get 'count'
      end
    end

    resources :stitch_types do

      member do
        resources :stitch_type_products, :only => [:show, :index]
      end

      collection do
        get 'count'
        get 'tree'
      end
    end
    root :to => 'stitches#index'
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  root to: redirect("/#{I18n.default_locale}", status: 302), as: :redirected_root
  match '*path', to: redirect {|params, request| "/#{I18n.default_locale}#{request.original_fullpath}"}, constraints: {path: /(?!(#{I18n.available_locales.join("|")})\/).*/}, format: false

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.


  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
