#!/bin/bash


#Collecting options
while getopts d:t: option
do
  case "${option}"
  in
          c) CLEAN_DB=true;;
  esac
done

if [ -z "$CLEAN_DB" ]; then
  CLEAN_DB=false
fi


if $CLEAN_DB; then
  echo "flushing local db"

  RAILS_ENV=production rake db:drop
  RAILS_ENV=production rake db:create
  RAILS_ENV=production rake db:migrate

fi

echo "Connecting to remote server"
taps pull postgres://deployer:d3pl0y3r@localhost/miles_production http://link:pochacco@miles.bluetouchdev.com:8080