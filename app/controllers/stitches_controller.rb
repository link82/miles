class StitchesController < ApplicationController

  before_filter :set_pagination, :only => [:index]

  # GET /stitches
  # GET /stitches.json
  def index
    @stitches = Stitch.paginate(:page => @__page, :per_page => @__limit).includes(:pictures)
      .updated(params[:updated].present?)
      .filterd_by_code(params[:code])
      .with_type(params[:stitch_type_id])
      .with_translations(Globalize.locale)
      .order("stitches.id ASC")

    x = {
      :source => "webapp",
      :user => "guest",
      :code => "",
      :year => "",
      :composition => "",
      :season_code => "",
      :stitch_type_codes => "",
      :supplier_codes => "",
      :yarn_codes => "",
      :product_codes => "",
      :brand_code => "",
      :machine_id => "",
      :thinness => "",
      :stretch => "",
      :vanise => "",
      :page => "1",
      :limit => "100",
      :locale => "it"
    }

    respond_to do |format|
      format.html # index.html.erb
      format.json #{ render json: @stitches }
    end
  end

  def archive

    kind =  (params[:kind].nil? || !["all_stitches",
                                    "updated_stitches",
                                    "all_products_images",
                                    "all_products_sketches",
                                    "updated_products_images",
                                    "updated_products_sketches",
                                    "backup",
                                    "backup_test"].include?(params[:kind]) ? "updated_stitches" : params[:kind])

    if Delayed::Job.count == 0
      Archive.delay.call_rake "archive:#{kind}"
      flash[:notice] = 'Procedure started in background'
    else
      flash[:warning] = 'Background procedure/s already running, please wait until completed'
    end

    respond_to do |format|
      format.html do
        redirect_to request.referer.nil? ? root_url : request.referer
      end
      format.json { render json: {:error => "Job started"}}
    end

  end

  def archives_count

    @count = 0
    base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
    directory = File.expand_path("#{base_path}/archives/")
    archives = ["stitches_images","products_images","products_sketches"]

    if params[:archive].blank? || !archives.index(params[:archive])

      respond_to do |format|
        format.html do
          flash[:warning] = "Invalid archive type"
          redirect_to root_url
        end
        format.json { render json: {:error => "Invalid archive type"}}
      end
      return
    end

    human_name = params[:archive].gsub("_"," ").capitalize

    Dir[File.join(directory, "#{params[:archive]}*.zip")].each do |file|
      @count +=1
    end

    respond_to do |format|
      format.html do
        flash[:notice] = "#{human_name} are splitted into #{@count} archives"
        redirect_to root_url
      end
      format.json { render json: {:count => @count}}
    end

  end



  def count
    @count = Stitch.count

    respond_to do |format|
      format.html do
        flash[:warning] = 'Not available'
        redirect_to root_url
      end
      format.json { render json: {:count => @count}}
    end

  end

  # GET /stitches/1
  # GET /stitches/1.json
  def show
    @stitch = Stitch.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json #{ render json: @stitch }
    end
  end

  # GET /stitches/new
  # GET /stitches/new.json
  def new
    @stitch = Stitch.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stitch }
    end
  end

  # GET /stitches/1/edit
  def edit
    @stitch = Stitch.find(params[:id])
  end

  # POST /stitches
  # POST /stitches.json
  def create
    @stitch = Stitch.new(params[:stitch])

    respond_to do |format|
      if @stitch.save
        format.html { redirect_to @stitch, notice: 'Stitch was successfully created.' }
        format.json { render json: @stitch, status: :created, location: @stitch }
      else
        format.html { render action: "new" }
        format.json { render json: @stitch.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stitches/1
  # PUT /stitches/1.json
  def update
    @stitch = Stitch.find(params[:id])

    respond_to do |format|
      if @stitch.update_attributes(params[:stitch])
        format.html { redirect_to @stitch, notice: 'Stitch was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stitch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stitches/1
  # DELETE /stitches/1.json
  def destroy
    @stitch = Stitch.find(params[:id])
    @stitch.destroy

    respond_to do |format|
      format.html { redirect_to stitches_url }
      format.json { head :no_content }
    end
  end
end
