class StitchTypesController < ApplicationController

  before_filter :set_pagination, :only => [:index]

  # GET /stitch_types
  # GET /stitch_types.json
  def index
    @stitch_types = StitchType.paginate(:page => @__page, :per_page => @__limit)
      .with_parent(params[:parent_id])
      .with_translations(Globalize.locale)
      .order("stitch_types.id ASC")
      .includes(:stitch_type_translations)

    respond_to do |format|
      format.html # index.html.erb
      format.json #{ render json: @stitch_types }
    end
  end

  def count
    @count = StitchType.count

    respond_to do |format|
      format.html do
        flash[:warning] = 'Not available'
        redirect_to root_url
      end
      format.json { render json: {:count => @count}}
    end

  end

  def tree

    @roots = StitchType.roots

    respond_to do |format|
      format.html { render "tree", :layout => "svg"}
    end

  end

  # GET /stitch_types/1
  # GET /stitch_types/1.json
  def show
    @stitch_type = StitchType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stitch_type }
    end
  end

  # GET /stitch_types/new
  # GET /stitch_types/new.json
  def new
    @stitch_type = StitchType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stitch_type }
    end
  end

  # GET /stitch_types/1/edit
  def edit
    @stitch_type = StitchType.find(params[:id])
  end

  # POST /stitch_types
  # POST /stitch_types.json
  def create
    @stitch_type = StitchType.new(params[:stitch_type])

    respond_to do |format|
      if @stitch_type.save
        format.html { redirect_to @stitch_type, notice: 'Stitch type was successfully created.' }
        format.json { render json: @stitch_type, status: :created, location: @stitch_type }
      else
        format.html { render action: "new" }
        format.json { render json: @stitch_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stitch_types/1
  # PUT /stitch_types/1.json
  def update
    @stitch_type = StitchType.find(params[:id])

    respond_to do |format|
      if @stitch_type.update_attributes(params[:stitch_type])
        format.html { redirect_to @stitch_type, notice: 'Stitch type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stitch_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stitch_types/1
  # DELETE /stitch_types/1.json
  def destroy
    @stitch_type = StitchType.find(params[:id])
    @stitch_type.destroy

    respond_to do |format|
      format.html { redirect_to stitch_types_url }
      format.json { head :no_content }
    end
  end
end
