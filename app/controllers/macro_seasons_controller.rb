class MacroSeasonsController < ApplicationController

  before_filter :set_pagination, :only => [:index]

  # GET /macro_seasons
  # GET /macro_seasons.json
  def index
    @macro_seasons = MacroSeason.paginate(:page => @__page, :per_page => @__limit)
      .order("id ASC")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @macro_seasons }
    end
  end

  def count
    @count = MacroSeason.count

    respond_to do |format|
      format.html do
        flash[:warning] = 'Not available'
        redirect_to root_url
      end
      format.json { render json: {:count => @count}}
    end

  end

  # GET /macro_seasons/1
  # GET /macro_seasons/1.json
  def show
    @macro_season = MacroSeason.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @macro_season }
    end
  end

  # GET /macro_seasons/new
  # GET /macro_seasons/new.json
  def new
    @macro_season = MacroSeason.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @macro_season }
    end
  end

  # GET /macro_seasons/1/edit
  def edit
    @macro_season = MacroSeason.find(params[:id])
  end

  # POST /macro_seasons
  # POST /macro_seasons.json
  def create
    @macro_season = MacroSeason.new(params[:macro_season])

    respond_to do |format|
      if @macro_season.save
        format.html { redirect_to @macro_season, notice: 'Macro season was successfully created.' }
        format.json { render json: @macro_season, status: :created, location: @macro_season }
      else
        format.html { render action: "new" }
        format.json { render json: @macro_season.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /macro_seasons/1
  # PUT /macro_seasons/1.json
  def update
    @macro_season = MacroSeason.find(params[:id])

    respond_to do |format|
      if @macro_season.update_attributes(params[:macro_season])
        format.html { redirect_to @macro_season, notice: 'Macro season was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @macro_season.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /macro_seasons/1
  # DELETE /macro_seasons/1.json
  def destroy
    @macro_season = MacroSeason.find(params[:id])
    @macro_season.destroy

    respond_to do |format|
      format.html { redirect_to macro_seasons_url }
      format.json { head :no_content }
    end
  end
end
