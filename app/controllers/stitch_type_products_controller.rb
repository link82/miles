class StitchTypeProductsController < ApplicationController

	before_filter :set_pagination, :only => [:index]

  # GET /stitch_type_products
  # GET /stitch_type_products.json
  def index
    @stitch_type_products = StitchTypeProduct.paginate(:page => @__page, :per_page => @__limit)
      .with_stitch_type_id(params[:stitch_type_id])
      .order("id ASC")

    respond_to do |format|
      format.html # index.html.erb
      format.json #{ render json: @stitch_type_products }
    end
  end

  def count
    @count = StitchTypeProduct.count

    respond_to do |format|
      format.html do
        flash[:warning] = 'Not available'
        redirect_to root_url
      end
      format.json { render json: {:count => @count}}
    end

  end

  # GET /stitch_type_products/1
  # GET /stitch_type_products/1.json
  def show
    @stitch_type_product = StitchTypeProduct.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stitch_type_product }
    end
  end

  # GET /stitch_type_products/new
  # GET /stitch_type_products/new.json
  def new
    @stitch_type_product = StitchTypeProduct.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stitch_type_product }
    end
  end

  # GET /stitch_type_products/1/edit
  def edit
    @stitch_type_product = StitchTypeProduct.find(params[:id])
  end

  # POST /stitch_type_products
  # POST /stitch_type_products.json
  def create
    @stitch_type_product = StitchTypeProduct.new(params[:stitch_type_product])

    respond_to do |format|
      if @stitch_type_product.save
        format.html { redirect_to @stitch_type_product, notice: 'Stitch type product was successfully created.' }
        format.json { render json: @stitch_type_product, status: :created, location: @stitch_type_product }
      else
        format.html { render action: "new" }
        format.json { render json: @stitch_type_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stitch_type_products/1
  # PUT /stitch_type_products/1.json
  def update
    @stitch_type_product = StitchTypeProduct.find(params[:id])

    respond_to do |format|
      if @stitch_type_product.update_attributes(params[:stitch_type_product])
        format.html { redirect_to @stitch_type_product, notice: 'Stitch type product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stitch_type_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stitch_type_products/1
  # DELETE /stitch_type_products/1.json
  def destroy
    @stitch_type_product = StitchTypeProduct.find(params[:id])
    @stitch_type_product.destroy

    respond_to do |format|
      format.html { redirect_to stitch_type_products_url }
      format.json { head :no_content }
    end
  end


end
