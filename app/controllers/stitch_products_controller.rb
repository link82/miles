class StitchProductsController < ApplicationController

  before_filter :set_pagination, :only => [:index]

  # GET /stitch_products
  # GET /stitch_products.json
  def index
    @stitch_products = StitchProduct.paginate(:page => @__page, :per_page => @__limit)
      .order("id ASC")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stitch_products }
    end
  end

  def count
    @count = StitchProduct.count

    respond_to do |format|
      format.html do
        flash[:warning] = 'Not available'
        redirect_to root_url
      end
      format.json { render json: {:count => @count}}
    end

  end
  
  # GET /stitch_products/1
  # GET /stitch_products/1.json
  def show
    @stitch_product = StitchProduct.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stitch_product }
    end
  end

  # GET /stitch_products/new
  # GET /stitch_products/new.json
  def new
    @stitch_product = StitchProduct.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stitch_product }
    end
  end

  # GET /stitch_products/1/edit
  def edit
    @stitch_product = StitchProduct.find(params[:id])
  end

  # POST /stitch_products
  # POST /stitch_products.json
  def create
    @stitch_product = StitchProduct.new(params[:stitch_product])

    respond_to do |format|
      if @stitch_product.save
        format.html { redirect_to @stitch_product, notice: 'Stitch product was successfully created.' }
        format.json { render json: @stitch_product, status: :created, location: @stitch_product }
      else
        format.html { render action: "new" }
        format.json { render json: @stitch_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stitch_products/1
  # PUT /stitch_products/1.json
  def update
    @stitch_product = StitchProduct.find(params[:id])

    respond_to do |format|
      if @stitch_product.update_attributes(params[:stitch_product])
        format.html { redirect_to @stitch_product, notice: 'Stitch product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stitch_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stitch_products/1
  # DELETE /stitch_products/1.json
  def destroy
    @stitch_product = StitchProduct.find(params[:id])
    @stitch_product.destroy

    respond_to do |format|
      format.html { redirect_to stitch_products_url }
      format.json { head :no_content }
    end
  end
end
