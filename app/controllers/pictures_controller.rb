class PicturesController < ApplicationController


  before_filter :set_pagination, :only => [:index]


  def download_url

    s3_config ||= YAML.load(ERB.new(File.read("#{Rails.root}/config/s3.yml")).result)[Rails.env]

    s3 = AWS::S3.new(
        :access_key_id => s3_config['access_key_id'],
        :secret_access_key => s3_config['secret_access_key']
      )

    bucket = s3.buckets['bluetouchdev-backups']
    obj = bucket.objects['miles/images/images.zip']
    @url = obj.url_for(:read, :expire => 10 * 60)

    respond_to do |format|
      format.json {render json:{:secure_url => Picture.s3_archive_url.to_s}, status: :ok}
      format.html {redirect_to Picture.s3_archive_url.to_s}
    end
  end


  # GET /pictures/export.json
  def export
    

    respond_to do |format|
      format.html do
        flash[:notice] = "Not available"
        redirect_to root_url
      end
      format.json do
        @pictures = Picture.order("id ASC").all
        render json: @pictures
      end
    end
  end

  # GET /pictures
  # GET /pictures.json
  def index
    @pictures = Picture.paginate(:page => @__page, :per_page => @__limit)
      .for_stitch(params[:stitch_id])
      .order(request.format == 'json' ? "id ASC" : 'code ASC')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pictures }
    end
  end

  def count
    @count = Picture.count

    respond_to do |format|
      format.html do
        flash[:warning] = 'Not available'
        redirect_to root_url
      end
      format.json { render json: {:count => @count}}
    end

  end

  # GET /pictures/1
  # GET /pictures/1.json
  def show
    @picture = Picture.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @picture }
    end
  end

  # GET /pictures/new
  # GET /pictures/new.json
  def new
    @picture = Picture.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @picture }
    end
  end

  # GET /pictures/1/edit
  def edit
    @picture = Picture.find(params[:id])
  end

  # POST /pictures
  # POST /pictures.json
  def create
    @picture = Picture.new(params[:picture])

    respond_to do |format|
      if @picture.save
        format.html { redirect_to @picture, notice: 'Picture was successfully created.' }
        format.json { render json: @picture, status: :created, location: @picture }
      else
        format.html { render action: "new" }
        format.json { render json: @picture.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pictures/1
  # PUT /pictures/1.json
  def update
    @picture = Picture.find(params[:id])

    respond_to do |format|
      if @picture.update_attributes(params[:picture])
        format.html { redirect_to @picture, notice: 'Picture was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @picture.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pictures/1
  # DELETE /pictures/1.json
  def destroy
    @picture = Picture.find(params[:id])
    @picture.destroy

    respond_to do |format|
      format.html { redirect_to pictures_url }
      format.json { head :no_content }
    end
  end 
end
