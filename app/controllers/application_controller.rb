class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :prepend_view_paths #, :unless => :devise_controller?
  before_filter :set_locale
  #before_filter :cors_set_access_control_headers
  after_filter :cors_set_access_control_headers

  helper_method :queue

  def set_pagination
    @__page = (!params[:page].blank? && is_number(params[:page])) ? params[:page] : 1
    @__limit = (!params[:limit].blank? && is_number(params[:limit]) && (params[:limit].to_i <= 1000)) ? params[:limit].to_i : 20
  end

  def queue
    #stats ||= Sidekiq::Stats.new
    #stats.enqueued
    0
  end

  # ENABLE COORS

  # def cors_preflight_check
  #   if request.method == :options
  #     headers['Access-Control-Allow-Origin'] = '*'
  #     headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
  #     headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
  #     headers['Access-Control-Max-Age'] = '1728000'
  #     render :text => '', :content_type => 'text/plain'
  #   end
  # end

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = '*'
    #headers['Access-Control-Allow-Headers'] = %w{Origin Accept Content-Type X-Requested-With X-CSRF-Token}.join(',')
    #headers['Access-Control-Max-Age'] = "1728000"
  end


  def set_model_locale
    Globalize.locale = (params[:locale].blank? || !I18n.available_locales.include?(params[:locale].to_sym)) ? I18n.locale : params[:locale].to_sym
  end

  ###
  # I18n
  def set_locale
    if params[:locale].present? && I18n.available_locales.include?(params[:locale].to_sym)
      logger.info "*** Setting up locale to #{current_locale} ***"
      I18n.locale = current_locale
      Globalize.locale = current_locale
    else
      redirect_to root_path(locale: user_default_locale), status: 301
    end
  end

  def browser_locale
    request.env['HTTP_ACCEPT_LANGUAGE'].to_s.first(2)
  end

  def user_default_locale
    I18n.available_locales.include?(browser_locale.to_s) ? browser_locale.to_sym : I18n.default_locale.to_sym
  end

  def current_locale
    I18n.available_locales.include?( params[:locale].to_sym ) ? params[:locale].to_sym : user_default_locale
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge(options)
  end







  def is_number(s)
    s.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
  end

  def prepend_view_paths
    prepend_view_path "app/views/api/v1" #if params[:format] == 'json'
  end

end
