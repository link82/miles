class YarnsController < ApplicationController

  before_filter :set_pagination, :only => [:index]

  # GET /yarns
  # GET /yarns.json
  def index
    @yarns = Yarn.paginate(:page => @__page, :per_page => @__limit)
      .for_supplier(params[:supplier_code])
      .order("id ASC")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @yarns }
    end
  end

  def count
    @count = Yarn.count

    respond_to do |format|
      format.html do
        flash[:warning] = 'Not available'
        redirect_to root_url
      end
      format.json { render json: {:count => @count}}
    end

  end

  # GET /yarns/1
  # GET /yarns/1.json
  def show
    @yarn = Yarn.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @yarn }
    end
  end

  # GET /yarns/new
  # GET /yarns/new.json
  def new
    @yarn = Yarn.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @yarn }
    end
  end

  # GET /yarns/1/edit
  def edit
    @yarn = Yarn.find(params[:id])
  end

  # POST /yarns
  # POST /yarns.json
  def create
    @yarn = Yarn.new(params[:yarn])

    respond_to do |format|
      if @yarn.save
        format.html { redirect_to @yarn, notice: 'Yarn was successfully created.' }
        format.json { render json: @yarn, status: :created, location: @yarn }
      else
        format.html { render action: "new" }
        format.json { render json: @yarn.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /yarns/1
  # PUT /yarns/1.json
  def update
    @yarn = Yarn.find(params[:id])

    respond_to do |format|
      if @yarn.update_attributes(params[:yarn])
        format.html { redirect_to @yarn, notice: 'Yarn was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @yarn.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /yarns/1
  # DELETE /yarns/1.json
  def destroy
    @yarn = Yarn.find(params[:id])
    @yarn.destroy

    respond_to do |format|
      format.html { redirect_to yarns_url }
      format.json { head :no_content }
    end
  end
end
