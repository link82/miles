object @stitch_type

attributes :id, :code, :description, :full_description, :parent_id, :created_at, :updated_at

node :language do |product|
  product.language
end

child :stitch_type_products do |stitch_type|
	extends "api/v1/stitch_type_products/show"
end
