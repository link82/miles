object @stitch

attributes :id, :year, :season_code, :machine_id, :composition, :brand_id, :thinness
attributes :stretch, :vanise, :description, :code, :yarn_codes, :product_codes, :stitch_type_codes
attributes :stitch_type_id, :created_at, :updated_at, :updated_images, :supplier_codes

node :language do |product|
  product.language
end

node :yarn do |stitch|
	stitch.yarn
end


child :pictures do |picture|
	attributes :id, :code, :description, :stitch_id, :created_at, :updated_at

	node :language do |picture|
  	picture.language
	end

	node(:updated) do |picture|
		picture.updated?
	end

	node(:detailed_picture) do |picture|
		picture.image_file_name.include? "Dettaglio"
	end

	node(:url) do |picture|
		unless picture.nil?
			picture.image.url(:medium)
		end
	end

end
