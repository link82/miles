object @stitch_type_product

	attributes :id
	attributes :brand, :category, :description, :model, :product_code
	#attributes :product_id
	attributes :season, :stitch_type_code, :stitch_type_id
  attributes :updated_picture, :updated_sketch, :year
  attributes :created_at, :updated_at

  node :language do |product|
    product.language
  end

	node(:sketch_url) do |product|
		unless product.sketch_file_name.nil?
			product.sketch.url(:original)
		end
	end

	node(:image_url) do |product|
		unless product.image_file_name.nil?
			product.image.url(:medium)
		end
	end

