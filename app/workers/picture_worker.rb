class PictureWorker
  #include Sidekiq::Worker
  #  sidekiq_options retry: false
  #  sidekid_options queue: "default"
  def perform (stitch_code, base_path)

    stitch = Stitch.with_code(stitch_code.to_s)

    unless stitch.nil?

      puts "---> Deleting pictures for stitch #{stitch.id}\n"
      Picture.where(:stitch_id => stitch.id).each{|d| d.destroy}
      puts "---> Importing new pictures from path #{base_path}\n"

      Dir.glob(File.join(base_path, "/#{stitch_code.to_s}.*")) do |file_path|
        puts "---> Going to import #{file_path} for stitch with id #{stitch.id}\n"

        # create new model for every picture found and save it to db
        open(file_path) do |f|
          pict = Picture.new(:stitch_id => stitch.id, :description => File.basename(file_path),
                             :image => f)
          # a side affect of saving is that paperclip transformation will
          # happen
          pict.save!
        end


        # Move processed image somewhere else or just remove it. It is
        # necessary as there is a risk of "double import"
        #FileUtils.mv(file_path, "....")
        #FileUtils.rm(file_path)
      end
    else
      puts "---> Invalid missing stitch\n"
    end


  end
end