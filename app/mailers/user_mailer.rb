class UserMailer < ActionMailer::Base
  default from: "mailer@bluetouchdev.com"

  def notify_completed(task, message, recipient)

  	@task = task
  	@message = message
    mail(:to => recipient, :bcc => "info@bluetouchdev.com", :subject => "Miles stitch research - Automatic mailer")
  end

end
