class StitchYarn < I18nModel
  attr_accessible :stitch_id, :yarn_id

  belongs_to :stitch
  belongs_to :yarn

  def self.with_code(code)
    self.all(:conditions => {:code => code}, :limit => 1).first
  end

end
