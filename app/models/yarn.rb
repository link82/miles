class Yarn < I18nModel
  attr_accessible :description, :supplier_code, :supplier_name, :code

  has_many :stitch_yarns
  has_many :stitches, :through => :stitch_yarns
  belongs_to :supplier, :foreign_key => :supplier_code , :primary_key => :code

  scope :for_supplier, lambda{ |_supplier_code|
  	unless _supplier_code.blank?
  		{:conditions => ["yarns.supplier_code = ?",_supplier_code]}
  	else
  		{}
  	end
  }

  #def supplier
  #	Supplier.where("suppliers.code = ?",self.supplier_code.to_s).first
  #end

  def self.with_code(code)
    self.all(:conditions => {:code => code}, :limit => 1).first
  end

end
