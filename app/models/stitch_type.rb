class StitchType < I18nModel
  attr_accessible :description, :full_description, :leaf, :parent_id, :code
  translates :description, :full_description

  #has_ancestry
  has_many :stitches
  has_many :childrens, :class_name => 'StitchType', :foreign_key => :parent_id, :primary_key => :id
  has_many :stitch_type_products
  has_many :products, :through => :stitch_type_products
  has_many :stitch_type_translations, :class_name => 'StitchType::Translation'

  belongs_to :parent, :class_name => 'StitchType', :foreign_key => :parent_id

  def self.with_code(code)
    self.all(:conditions => {:code => code.to_s}, :limit => 1).first
  end

  def name
  	self.description
  end

  def entry_type
  	"node"
  end

  def has_children?
    self.childrens.size > 0
  end

  def has_products?
    self.stitch_type_products.size > 0
  end

  scope :with_parent , lambda{ |parent_id|
    if parent_id.blank?
      {}
    elsif (parent_id != '-1')
      {:conditions => ["stitch_types.parent_id = ?",parent_id.to_i]}
    else
      {:conditions => ["stitch_types.parent_id IS NULL"]}
    end
  }



end
