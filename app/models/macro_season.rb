class MacroSeason < I18nModel
  attr_accessible :code, :description

  has_many :stitches, :foreign_key => :code, :primary_key => :season_code
end
