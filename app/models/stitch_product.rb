class StitchProduct < I18nModel
  attr_accessible :brand, :category_id, :description, :model, :product_id, :stitch_id, :code

  #belongs_to :category
  belongs_to :product
  belongs_to :stitch


  def self.with_code(code)
    self.all(:conditions => {:code => code}, :limit => 1).first
  end

end
