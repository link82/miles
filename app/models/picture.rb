class Picture < I18nModel
  attr_accessible :description, :stitch_id, :image, :code

  belongs_to :stitch

  has_attached_file :image,
      :path => ":rails_root/public/system/:attachment/:style/:id.:extension",
      :url => "/system/:attachment/:style/:id.:extension",
      :styles =>{
        :original => {:geometry =>"100%", :format => :jpg, :density => 72, :processor => [:screen_density]},
        :medium => {:geometry => "584^x584", :format => :jpg, :density => 72, :processor => [:screen_density]}
      }
      #584
      #:thumb => ["200^x200", :jpg]

  #validates_attachment :image, :presence => true,
  #  :content_type => { :content_type => ["image/jpg","image/png"] },
  #  :size => { :in => 0..10.kilobytes }

  def self.for_code(code)
    self.all(:conditions => {:code => code})
  end

  scope :updated , lambda{
      {:joins => [:stitch], :conditions => ["stitches.updated_images = ?",true]}
  }

  scope :for_stitch , lambda{ |stitch_id|
    if stitch_id.blank?
      {}
    else
      {:conditions => ["pictures.stitch_id = ?",stitch_id.to_i]}
    end
  }

  def self.with_code(code)
    self.all(:conditions => {:code => code}, :limit => 1).first
  end

  def updated?
    self.stitch.updated_images
  end

  def self.s3_archive_url

    s3 = AWS::S3.new(
        :access_key_id => self.s3_config['access_key_id'],
        :secret_access_key => self.s3_config['secret_access_key']
      )

    bucket = s3.buckets['bluetouchdev-backups']
    obj = bucket.objects['miles/images/images.zip']
    url = obj.url_for(:read, :expire => 10 * 60)

    url
  end

  def self.import(stitch_code, base_path)
    import_from_stitch nil, stitch_code, base_path
  end
  def self.import_from_stitch(stitch, stitch_code, base_path)
    stitch = Stitch.with_code(stitch_code.to_s) if stitch.nil?
    pictures = Picture.find_all_by_code(stitch_code.to_s)


    unless stitch.nil?
      Delayed::Worker.logger.debug("Stitch #{stitch_code} found")
      unless pictures.nil?
        pictures.each{|p| p.destroy}
      end
      stitch_code = stitch.code
      Delayed::Worker.logger.debug("Base path. #{base_path}/#{stitch_code.to_s}.*)")
      #immagine normale
      Dir.glob(File.join(base_path, "/#{stitch_code.to_s}.*")) do |file_path|
        puts "---> Going to import #{file_path} for stitch with id #{stitch.id}\n"

        # create new model for every picture found and save it to db
        open(file_path) do |f|
          pict = Picture.new(:stitch_id => stitch.id, :description => File.basename(file_path),
                             :image => f, :code => stitch_code.to_s)
          pict.save!
        end
      end

      Delayed::Worker.logger.debug("Base path. #{base_path}/#{stitch_code.to_s}_Dettaglio.*)")
      #Dettaglio
      Dir.glob(File.join(base_path, "/#{stitch_code.to_s}_Dettaglio.*")) do |file_path|
        puts "---> Going to import #{file_path} for stitch with id #{stitch.id}\n"

        # create new model for every picture found and save it to db
        open(file_path) do |f|
          pict = Picture.new(:stitch_id => stitch.id, :description => File.basename(file_path),
                             :image => f, :code => stitch_code.to_s)
          pict.save!
        end
      end

      Delayed::Worker.logger.debug("Base path. #{base_path}/#{stitch_code.to_s}_dettaglio.*)")
      #dettaglio
      Dir.glob(File.join(base_path, "/#{stitch_code.to_s}_dettaglio.*")) do |file_path|
        puts "---> Going to import #{file_path} for stitch with id #{stitch.id}\n"

        # create new model for every picture found and save it to db
        open(file_path) do |f|
          pict = Picture.new(:stitch_id => stitch.id, :description => File.basename(file_path),
                             :image => f, :code => stitch_code.to_s)
          pict.save!
        end
      end


    else
      puts "---> Invalid / missing stitch \n"
    end

  end

  def self.fix
    Picture.find_each do |p|
      unless p.stitch.nil?
        puts "Updating code to #{p.stitch.code} for picture #{p.id}"
        p.code = p.stitch.code
        p.save!
      else
        puts "Missing stitch for picture #{p.id}"
      end
    end
  end

  def self.reconnect(stitch_code)

    stitch = Stitch.with_code(stitch_code.to_s)
    pictures = Picture.for_code(stitch_code.to_s)

    unless stitch.nil?
      pictures.each do |p|
        #puts "Reconnecting picture #{p.id} to stitch #{stitch.code} - #{stitch.id}"
        p.stitch_id = stitch.id
        p.save!
      end
    end

  end

  protected

  def self.s3_config
    @@s3_config ||= YAML.load(ERB.new(File.read("#{Rails.root}/config/s3.yml")).result)[Rails.env]
  end

end
