class StitchTypeProduct < I18nModel
  attr_accessible :brand, :category, :description, :model, :product_code
  attr_accessible :product_id, :season, :stitch_type_code, :stitch_type_id, :year
  attr_accessible :updated_picture, :updated_sketch

  attr_accessible :image, :sketch

  belongs_to :product
  belongs_to :stitch_type

  scope :with_stitch_type_id , lambda{ |stitch_type_id|
    if stitch_type_id.blank?
      {}
    else
      {:conditions => ["stitch_type_products.stitch_type_id = ?",stitch_type_id.to_i]}
    end
  }

  has_attached_file :image,
      :path => ":rails_root/public/system/products/:attachment/:style/:id.:extension",
      :url => "/system/products/:attachment/:style/:id.:extension",
      :styles =>{
        :original => {:geometry =>"100%", :format => :jpg, :density => 72, :processor => [:screen_density]},
        :medium => {:geometry => "500^x750", :format => :jpg, :density => 72, :processor => [:screen_density]}
      }


  has_attached_file :sketch,
      :path => ":rails_root/public/system/products/:attachment/:style/:id.:extension",
      :url => "/system/products/:attachment/:style/:id.:extension"

  def self.s3_archive_url

    s3 = AWS::S3.new(
        :access_key_id => self.s3_config['access_key_id'],
        :secret_access_key => self.s3_config['secret_access_key']
      )

    bucket = s3.buckets['bluetouchdev-backups']
    obj = bucket.objects['miles/images/images.zip']
    url = obj.url_for(:read, :expire => 10 * 60)

    url
  end

  def self.with_stitch_type_and_product_code(s_code,p_code)
    self.all(:conditions => {:stitch_type_code => s_code, :product_code => p_code}, :limit => 1).first
  end

  def self.find_or_initialize_by_stitch_type_and_product_code(s_code,p_code)
  	x = with_stitch_type_and_product_code(s_code,p_code)
  	x = StitchTypeProduct.new({:stitch_type_code => s_code, :product_code => p_code}) if x.nil?
  	x
  end


  #picture import management

  def import_images

    stitch_type = StitchType.with_code(self.stitch_type_code.to_s)
    puts "Importing / updating images for product #{self.product_code} (stitch type #{stitch_type.id})"

    unless stitch_type.nil?

      if self.updated_picture
        #immagine normale
        Dir.glob(File.join(picture_base_path, "/#{self.product_code.to_s.downcase}.jpg")) do |file_path|
          puts "---> Going to import product image #{file_path} for product #{self.id}\n"

          # create new model for every picture found and save it to db
          open(file_path) do |f|
            self.image = f
            self.save!
          end
        end
      else
        puts "Picture not updated, keeping current version"
      end

      if self.updated_sketch
        #sketch
        Dir.glob(File.join(sketch_base_path, "/#{self.product_code.to_s.upcase}.JPG")) do |file_path|
          puts "---> Going to import product sketch #{file_path} for product #{self.id}\n"

          # create new model for every picture found and save it to db
          open(file_path) do |f|
            self.sketch = f
            self.save!
          end
        end
      else
        puts "Sketch not updated, keeping current version"
      end

    else
      puts "---> Invalid missing stitch type\n"
    end

  end

  private

  def sketch_base_path
    Rails.env == 'production' ? "/media/milnas/Schizzi/" : "#{Rails.root}/config/import/Schizzi"
  end

  def picture_base_path
    Rails.env == 'production' ? "/media/milnas/Foto/" : "#{Rails.root}/config/import/Foto"
  end

end
