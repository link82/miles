class Brand < I18nModel

  attr_accessible :description
  attr_accessible :code

  def self.with_code(code)
    self.all(:conditions => {:code => code}, :limit => 1).first
  end

end
