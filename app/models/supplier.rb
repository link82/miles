class Supplier < I18nModel
  attr_accessible :code, :description

  has_many :yarns , :class_name => 'Yarn', :foreign_key => :supplier_code , :primary_key => :code


  def self.with_code(code)
    self.all(:conditions => {:code => code}, :limit => 1).first
  end

end
