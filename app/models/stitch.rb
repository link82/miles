class Stitch < I18nModel
  attr_accessible :brand_id, :composition, :description, :machine_id, :season_code, :stretch, :thinness
  attr_accessible :vanise, :year, :code, :yarn_codes, :product_codes, :stitch_type_codes, :stitch_type_id
  attr_accessible :updated_images, :supplier_codes

  translates :description, :composition

  belongs_to :machine
  belongs_to :brand
  belongs_to :stitch_type

  belongs_to :macro_season, :foreign_key => :season_code, :primary_key => :code

  has_many :stitch_products
  has_many :products, :through => :stitch_products
  has_many :pictures

  has_many :stitch_yarns
  has_many :yarns, :through => :stitch_yarns

  scope :updated, lambda{|updated_val|
    if updated_val
      {:conditions => ["stitches.updated_images = ?",true]}
    else
      {}
    end
  }

  scope :with_type , lambda{ |stitch_type_id|
    if stitch_type_id.blank?
      {}
    else
      {:conditions => ["stitches.stitch_type_id = ?",stitch_type_id.to_i]}
    end
  }

  scope :filterd_by_code, lambda{ |filter_code|
    if filter_code.blank?
      {}
    else
      {:conditions => ["stitches.code = ?",filter_code]}
    end
  }

  def self.with_code(code)
    self.all(:conditions => {:code => code}, :limit => 1).first
  end

  def yarn
    self.yarns.first if self.yarns.any?
  end

  def name
    self.description
  end

end
