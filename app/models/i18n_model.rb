class I18nModel < ActiveRecord::Base
  self.abstract_class = true
  def language
    Globalize.locale
  end

  def as_json(options = { })
    super({:methods => [:language]}.merge(options))
  end

end
