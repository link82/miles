class Machine < I18nModel
  attr_accessible :description, :code

  has_many :stitches

  def self.with_code(code)
    self.all(:conditions => {:code => code}, :limit => 1).first
  end

end
