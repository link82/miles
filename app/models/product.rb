class Product < I18nModel
  attr_accessible :description, :code

  has_many :stitch_products
  has_many :stitches, :through => :stitch_products

  has_many :stitch_type_products
  has_many :stitch_types, :through => :stitch_type_products


  def self.with_code(code)
    self.all(:conditions => {:code => code}, :limit => 1).first
  end

end
