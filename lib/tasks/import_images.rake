namespace :import do

  desc "import all images from SOURCE_DIR folder"
  task :images => :environment do

    stitch_code = ENV["STITCH_CODE"]
    # get all images from given folder
    Dir.glob(File.join(ENV["SOURCE_DIR"], "*")) do |file_path|
      # create new model for every picture found and save it to db
      open(file_path) do |f|
        pict = Picture.new(:stitch_id => stitch_code, :description => File.basename(file_path),
                           :image => f)
        # a side affect of saving is that paperclip transformation will
        # happen
        pict.save!
      end


      # Move processed image somewhere else or just remove it. It is
      # necessary as there is a risk of "double import"
      #FileUtils.mv(file_path, "....")
      #FileUtils.rm(file_path)
    end
  end
end
#rake import:images SOURCE_DIR=~/my_images/to/be/imported

#~/Desktop/iOS\ Projects/progetti/Miles/import_folder/immagini
