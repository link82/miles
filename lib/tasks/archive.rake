require 'rubygems'
require 'zip'


def subjects
  @_s ||= {
    :all_stitches => 'Archiviazione immagini punti',
    :updated_stitches => 'Archiviazione immagini punti aggiornate',
    :all_products_images => 'Archiviazione immagini prodotti',
    :all_products_sketches => 'Archiviazione bozzetti prodotti',
    :updated_products_images => 'Archiviazione immagini prodotti aggiornate',
    :updated_products_sketches => 'Archiviazione bozzetti prodotti aggiornati',
    :mail => 'Email di test su task archiviazione',
    :backup => 'Archiviazione backup immagini punti su Amazon S3',
    :backup_test => 'Verifica creazione backup su Amazon S3'
  }
  @_s
end

def admin_email
  'link82@gmail.com'
end

namespace :archive do

  task :mail => :environment do |t|
      puts "--> Sending email notification"
      UserMailer.notify_completed(t,subjects[t.name.gsub("archive:","").to_sym],admin_email).deliver
  end

  desc "create archive with all updated images"
  task :all_stitches => :environment do |t|

    archive_nr = 0
    counter = 0
    limit = 1000

    directory = File.expand_path("#{Rails.root}/public/system/images/medium")
    base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
    zip_updated_file_name = File.expand_path("#{base_path}/archives/updated_stitches_images.zip")


    if File.exists? zip_updated_file_name
      puts "Removing previous generated 'updated_images' archive"
      File.delete(zip_updated_file_name)
    end


    puts "Unsetting previous 'updated' stitches"
    Stitch.updated(true).each{|s| s.update_attributes({:updated_images => false})}


    files = []
    files << []

    Dir[File.join(directory, '**')].each do |file|

      if (counter < limit)

        files[archive_nr] << file
        counter+=1

      else
        #reset counter, increase archive nr, refresh archive name
        counter = 0
        archive_nr+=1
        files << []
      end

    end

    files.each_with_index do |archive, index|

      archive_nr = (index +1)
      base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
      zipfile_name = File.expand_path("#{base_path}/archives/stitches_images_#{archive_nr}.zip")

      if File.exists? zipfile_name
        puts "Deleting previous archive #{archive_nr}"
        File.delete(zipfile_name)
      end

      puts "Generating archive #{archive_nr}"

      Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
        archive.each do |file|
          #puts "Compressing image #{file} into archive #{archive_nr}"
          zipfile.add(file.sub(directory +'/', ''), file)
        end
      end
      File.chmod(0644,zipfile_name)
      puts "Compressed #{archive.count} images into #{zipfile_name}"

    end

    puts "--> Archive generation completed"


    if Rails.env == 'production'
      puts "--> Sending email notification"
      UserMailer.notify_completed(t,subjects[t.name.gsub("archive:","").to_sym],admin_email).deliver
    end

  end




  desc "create archive with all products images"
  task :all_products_images => :environment do |t|

    archive_nr = 0
    counter = 0
    limit = 1000

    directory = File.expand_path("#{Rails.root}/public/system/products/images/medium")
    base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
    zip_updated_file_name = File.expand_path("#{base_path}/archives/updated_products_images.zip")


    if File.exists? zip_updated_file_name
      puts "Removing previous generated 'updated_products_images' archive"
      File.delete(zip_updated_file_name)
    end


    puts "Unsetting previous 'updated_picture' products"
    StitchTypeProduct.where("updated_picture = ?",true).each{|s| s.update_attributes!({:updated_picture => false})}



    files = []
    files << []

    Dir[File.join(directory, '**')].each do |file|

      if (counter < limit)

        files[archive_nr] << file
        counter+=1

      else
        #reset counter, increase archive nr, refresh archive name
        counter = 0
        archive_nr+=1
        files << []
      end

    end

    files.each_with_index do |archive, index|

      archive_nr = (index +1)
      base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
      zipfile_name = File.expand_path("#{base_path}/archives/products_images_#{archive_nr}.zip")

      if File.exists? zipfile_name
        puts "Deleting previous products archive #{archive_nr}"
        File.delete(zipfile_name)
      end

      puts "Generating archive #{archive_nr}"

      Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
        archive.each do |file|
          #puts "Compressing image #{file} into archive #{archive_nr}"
          zipfile.add(file.sub(directory +'/', ''), file)
        end
      end
      File.chmod(0644,zipfile_name)
      puts "Compressed #{archive.count} images into #{zipfile_name}"

    end

    puts "--> Archive generation completed"


    if Rails.env == 'production'
      puts "--> Sending email notification"
      UserMailer.notify_completed(t,subjects[t.name.gsub("archive:","").to_sym],admin_email).deliver
    end

  end






  desc "create archive with all products sketches"
  task :all_products_sketches => :environment do |t|

    archive_nr = 0
    counter = 0
    limit = 1000

    directory = File.expand_path("#{Rails.root}/public/system/products/sketches/original")
    base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
    zip_updated_file_name = File.expand_path("#{base_path}/archives/updated_products_sketches.zip")


    if File.exists? zip_updated_file_name
      puts "Removing previous generated 'updated_products_sketches' archive"
      File.delete(zip_updated_file_name)
    end


    puts "Unsetting previous 'updated_picture' products"
    StitchTypeProduct.where("updated_sketch = ?",true).each{|s| s.update_attributes!({:updated_sketch => false})}



    files = []
    files << []

    Dir[File.join(directory, '**')].each do |file|

      if (counter < limit)

        files[archive_nr] << file
        counter+=1

      else
        #reset counter, increase archive nr, refresh archive name
        counter = 0
        archive_nr+=1
        files << []
      end

    end

    files.each_with_index do |archive, index|

      archive_nr = (index +1)
      base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
      zipfile_name = File.expand_path("#{base_path}/archives/products_sketches_#{archive_nr}.zip")

      if File.exists? zipfile_name
        puts "Deleting previous products sketches archive #{archive_nr}"
        File.delete(zipfile_name)
      end

      puts "Generating archive #{archive_nr}"

      Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
        archive.each do |file|
          #puts "Compressing image #{file} into archive #{archive_nr}"
          zipfile.add(file.sub(directory +'/', ''), file)
        end
      end
      File.chmod(0644,zipfile_name)
      puts "Compressed #{archive.count} sketches into #{zipfile_name}"

    end

    puts "--> Archive generation completed"


    if Rails.env == 'production'
      puts "--> Sending email notification"
      UserMailer.notify_completed(t,subjects[t.name.gsub("archive:","").to_sym],admin_email).deliver
    end

  end



  ########################################
  ######## UPDATED PICTURES TASKS ########
  ########################################


  desc "create archive with all updated stitches images"
  task :updated_stitches => :environment do |t|

    directory = File.expand_path("#{Rails.root}/public/system/images/medium")
    base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
    zipfile_name = File.expand_path("#{base_path}/archives/updated_stitches_images.zip")
    pics_ids = Picture.updated.pluck(:id)

    puts "Pictures directory: #{directory}"
    puts "Zip archive path: #{zipfile_name}"

    if File.exists? zipfile_name
      puts "Deleting previous archive"
      File.delete(zipfile_name)
    end

    #if there is no image it exits
    if pics_ids.count == 0


      if Rails.env == 'production'
        puts "--> Sending email notification"
        UserMailer.notify_completed(t,"No updated stitches picture to archive, exiting",admin_email).deliver
      end

      abort("--> No updated stitches picture to archive, exiting")
    end


    puts "--> Creating new archive"
    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|

      pics_ids.each do |pic|
        file_path = File.join(directory,"#{pic}.jpg")
        if File.exists? file_path
          puts "Compressing image #{pic}.jpg"
          zipfile.add("#{pic}.jpg",file_path)
        else
          puts "Skipping image #{pic}.jpg (not found)"
        end

      end
    end
    File.chmod(0644,zipfile_name)
    puts "--> Archive creation completed"

    if Rails.env == 'production'
      puts "--> Sending email notification"
      UserMailer.notify_completed(t,subjects[t.name.gsub("archive:","").to_sym],admin_email).deliver
    end

  end


  desc "create archive with all updated products images"
  task :updated_products_images => :environment do |t|

    directory = File.expand_path("#{Rails.root}/public/system/products/images/medium")
    base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
    zipfile_name = File.expand_path("#{base_path}/archives/updated_products_images.zip")
    pics_ids = StitchTypeProduct.where("updated_picture = ?",true).pluck(:id)

    puts "Pictures directory: #{directory}"
    puts "Zip archive path: #{zipfile_name}"

    if File.exists? zipfile_name
      puts "Deleting previous archive"
      File.delete(zipfile_name)
    end

    #if there is no image it exits
    if pics_ids.count == 0


      if Rails.env == 'production'
        puts "--> Sending email notification"
        UserMailer.notify_completed(t,"No updated products images to archive, exiting",admin_email).deliver
      end

      abort("--> No updated products images to archive, exiting")
    end


    puts "--> Creating new archive"
    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|

      pics_ids.each do |pic|
        file_path = File.join(directory,"#{pic}.jpg")
        if File.exists? file_path
          puts "Compressing image #{pic}.jpg"
          zipfile.add("#{pic}.jpg",file_path)
        else
          puts "Skipping image #{pic}.jpg (not found)"
        end

      end
    end
    File.chmod(0644,zipfile_name)
    puts "--> Archive creation completed"

    if Rails.env == 'production'
      puts "--> Sending email notification"
      UserMailer.notify_completed(t,subjects[t.name.gsub("archive:","").to_sym],admin_email).deliver
    end

  end


  desc "create archive with all updated products sketches"
  task :updated_products_sketches => :environment do |t|

    directory = File.expand_path("#{Rails.root}/public/system/products/sketches/original")
    base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
    zipfile_name = File.expand_path("#{base_path}/archives/updated_products_sketches.zip")
    pics_ids = StitchTypeProduct.where("updated_sketch = ?",true).pluck(:id)

    puts "Pictures directory: #{directory}"
    puts "Zip archive path: #{zipfile_name}"

    if File.exists? zipfile_name
      puts "Deleting previous archive"
      File.delete(zipfile_name)
    end

    #if there is no image it exits
    if pics_ids.count == 0


      if Rails.env == 'production'
        puts "--> Sending email notification"
        UserMailer.notify_completed(t,"No updated products sketches to archive, exiting",admin_email).deliver
      end

      abort("--> No updated products sketches to archive, exiting")
    end


    puts "--> Creating new archive"
    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|

      pics_ids.each do |pic|
        file_path = File.join(directory,"#{pic}.JPG")
        if File.exists? file_path
          puts "Compressing image #{pic}.JPG"
          zipfile.add("#{pic}.JPG",file_path)

        else
          puts "Skipping image #{pic}.JPG (not found)"
        end

      end
    end
    File.chmod(0644,zipfile_name)
    puts "--> Archive creation completed"

    if Rails.env == 'production'
      puts "--> Sending email notification"
      UserMailer.notify_completed(t,subjects[t.name.gsub("archive:","").to_sym],admin_email).deliver
    end

  end









  ########################################
  ########################################


  ########################################
  ######## AMAZON S3 BACKUP TASKS ########
  ########################################

  desc "Backup current image archive to Amazon S3 servers"
  task :backup_test => :environment do |t|

    require 'rubygems'
    require 'aws-sdk'

    AWS.config(access_key_id: 'AKIAI6OAN4ZBBFD4NH5A', secret_access_key: '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')

    bucket_name = 'bluetouchdev-backups'
    file_name = "#{Rails.root}/public/system/images/medium/10000.jpg"

    puts "Going to upload file #{file_name}"

    # Get an instance of the S3 interface.
    s3 = AWS::S3.new

    # Upload a file.
    key = "miles/images/#{File.basename(file_name)}"
    puts "Uploading file #{file_name} to bucket #{bucket_name} with key #{key}"
    s3.buckets[bucket_name].objects[key].write(:file => file_name)
    puts "Upload completed!!"

    if Rails.env == 'production'
      puts "--> Sending email notification"
      UserMailer.notify_completed(t,subjects[t.name.gsub("archive:","").to_sym],admin_email).deliver
    end

  end

  desc "Backup current image archive to Amazon S3 servers"
  task :backup => :environment do |t|

    require 'rubygems'
    require 'aws-sdk'

    AWS.config(access_key_id: 'AKIAI6OAN4ZBBFD4NH5A', secret_access_key: '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')

    bucket_name = 'bluetouchdev-backups'
    base_path = Rails.env.development? ? Rails.root.join("public","system") : "/media/milnas01"
    file_name = "#{base_path}/archives/stitches_images.zip"

    puts "Going to upload file #{file_name}"

    # Get an instance of the S3 interface.
    s3 = AWS::S3.new

    # Upload a file.
    key = "miles/images/#{File.basename(file_name)}"
    puts "Uploading file #{file_name} to bucket #{bucket_name} with key #{key}"
    s3.buckets[bucket_name].objects[key].write(:file => file_name)
    puts "Upload completed!!"

    if Rails.env == 'production'
      puts "--> Sending email notification"
      UserMailer.notify_completed(t,subjects[t.name.gsub("archive:","").to_sym],admin_email).deliver
    end

  end

  ########################################
  ########################################


end
