module Paperclip

  class ScreenDensity < Thumbnail

    def transformation_command
      super + " -density 72"
    end

   end
end