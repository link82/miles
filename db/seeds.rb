# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

subjects = {
  :all => 'Aggiornamento catalogo punti',
  :tipo_punto => 'Aggiornamento anagrafica Tipi punto',
  :fornitore => 'Aggiornamento anagrafica Fornitori',
  :marchio => 'Aggiornamento anagrafica Marchi',
  :filato => 'Aggiornamento anagrafica Filati',
  :macchina => 'Aggiornamento anagrafica Macchine',
  :prodotto => 'Aggiornamento anagrafica Prodotti',
  :stagione => 'Aggiornamento anagrafica Stagioni',
  :immagini => 'Aggiornamento anagrafica Immagini',
  :tipo_punto_prodotto => 'Aggiornamento anagrafica Prodotti per Tipi punto'
}



kind = ENV["kind"]
force_image_import = ENV["force_import_images"].nil? ? false : true

#used to inibit thumbs generation in dev environment
generate_thumbs = ENV["generate_thumbs"].nil? ? false : true

#avoid flushing all data, search for existing record and update that
clean_data = ENV["clean_data"].nil? ? false : true

fg_processing = ENV["fg_processing"].nil? ? false : true

#11616

if kind == 'all' || kind == 'tipo_punto'
  import_stitch_types(clean_data)
  import_i18n_for :stitch_type
end

if(kind == 'all' || kind == 'fornitore')
  import_suppliers(clean_data)
end

if(kind == 'all' || kind == 'marchio')
  import_brands(clean_data)
end

if(kind == 'all' || kind == 'filato')
  import_yarns(clean_data)
end

if(kind == 'all' || kind == 'macchina')
  import_machines(clean_data)
end

if(kind == 'all' || kind == 'prodotto')
  import_products(clean_data)
end

if(kind == 'all' || kind == 'stagione')
  import_seasons(clean_data)
end

# if(kind == 'all' || kind == 'immagini')
#   import_pictures(clean_data)
# end

if(kind == 'all' || kind == 'tipo_punto_prodotto')
  import_stitch_type_products(true,generate_thumbs,clean_data)
end

if(kind == 'all' || kind == 'punto')
  import_stitches(force_image_import,generate_thumbs,clean_data,fg_processing)
  import_i18n_for :stitch
end

if(kind == 'all' || kind = 'i18n')
  import_i18n_for :stitch_type
  import_i18n_for :stitch
end

puts '== Seeding completed =='

if Rails.env == 'production'
  UserMailer.notify_completed(kind,subjects[kind.to_sym],'link82@gmail.com').deliver
end
