# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140419092328) do

  create_table "brands", :force => true do |t|
    t.string   "code"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "brands", ["code"], :name => "index_brands_on_code", :unique => true

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "machines", :force => true do |t|
    t.string   "description"
    t.string   "code"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "machines", ["code"], :name => "index_machines_on_code", :unique => true

  create_table "macro_seasons", :force => true do |t|
    t.string   "code"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "macro_seasons", ["code"], :name => "index_macro_seasons_on_code", :unique => true

  create_table "pictures", :force => true do |t|
    t.string   "description"
    t.integer  "stitch_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "code"
  end

  add_index "pictures", ["code"], :name => "index_pictures_on_code"
  add_index "pictures", ["stitch_id"], :name => "index_pictures_on_stitch_id"

  create_table "products", :force => true do |t|
    t.string   "description"
    t.string   "code"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "products", ["code"], :name => "index_products_on_code", :unique => true

  create_table "stitch_products", :force => true do |t|
    t.integer  "stitch_id"
    t.integer  "product_id"
    t.integer  "category_id"
    t.string   "brand"
    t.string   "model"
    t.string   "description"
    t.string   "code"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "stitch_products", ["code"], :name => "index_stitch_products_on_code", :unique => true
  add_index "stitch_products", ["product_id"], :name => "index_stitch_products_on_product_id"
  add_index "stitch_products", ["stitch_id"], :name => "index_stitch_products_on_stitch_id"

  create_table "stitch_translations", :force => true do |t|
    t.integer  "stitch_id"
    t.string   "locale",      :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.text     "description"
    t.string   "composition"
  end

  add_index "stitch_translations", ["locale"], :name => "index_stitch_translations_on_locale"
  add_index "stitch_translations", ["stitch_id"], :name => "index_stitch_translations_on_stitch_id"

  create_table "stitch_type_products", :force => true do |t|
    t.string   "stitch_type_code"
    t.integer  "stitch_type_id"
    t.string   "product_code"
    t.integer  "product_id"
    t.text     "description"
    t.string   "category"
    t.integer  "year"
    t.string   "season"
    t.string   "brand"
    t.string   "model"
    t.boolean  "updated_picture"
    t.boolean  "updated_sketch"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "sketch_file_name"
    t.string   "sketch_content_type"
    t.integer  "sketch_file_size"
    t.datetime "sketch_updated_at"
  end

  add_index "stitch_type_products", ["product_id"], :name => "index_stitch_type_products_on_product_id"
  add_index "stitch_type_products", ["stitch_type_id"], :name => "index_stitch_type_products_on_stitch_type_id"

  create_table "stitch_type_translations", :force => true do |t|
    t.integer  "stitch_type_id"
    t.string   "locale",           :null => false
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "description"
    t.text     "full_description"
  end

  add_index "stitch_type_translations", ["locale"], :name => "index_stitch_type_translations_on_locale"
  add_index "stitch_type_translations", ["stitch_type_id"], :name => "index_stitch_type_translations_on_stitch_type_id"

  create_table "stitch_types", :force => true do |t|
    t.integer  "parent_id"
    t.string   "code"
    t.boolean  "leaf"
    t.string   "description"
    t.text     "full_description"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "stitch_types", ["code"], :name => "index_stitch_types_on_code", :unique => true
  add_index "stitch_types", ["parent_id"], :name => "index_stitch_types_on_parent_id"

  create_table "stitch_yarns", :force => true do |t|
    t.integer  "stitch_id"
    t.integer  "yarn_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "stitch_yarns", ["stitch_id"], :name => "index_stitch_yarns_on_stitch_id"
  add_index "stitch_yarns", ["yarn_id"], :name => "index_stitch_yarns_on_yarn_id"

  create_table "stitches", :force => true do |t|
    t.integer  "year"
    t.string   "season_code"
    t.integer  "machine_id"
    t.string   "composition"
    t.integer  "brand_id"
    t.integer  "thinness"
    t.boolean  "stretch"
    t.boolean  "vanise"
    t.string   "description"
    t.string   "code"
    t.string   "yarn_codes"
    t.string   "product_codes"
    t.string   "stitch_type_codes"
    t.integer  "stitch_type_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.boolean  "updated_images"
    t.string   "supplier_codes"
  end

  add_index "stitches", ["code"], :name => "index_stitches_on_code", :unique => true
  add_index "stitches", ["stitch_type_id"], :name => "index_stitches_on_stitch_type_id"
  add_index "stitches", ["updated_images"], :name => "index_stitches_on_updated_images"

  create_table "suppliers", :force => true do |t|
    t.string   "code"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "suppliers", ["code"], :name => "index_suppliers_on_code", :unique => true

  create_table "yarns", :force => true do |t|
    t.string   "supplier_code"
    t.string   "supplier_name"
    t.string   "description"
    t.string   "code"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "yarns", ["code"], :name => "index_yarns_on_code", :unique => true

end
