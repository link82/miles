class CreateMachines < ActiveRecord::Migration
  def change
    create_table :machines do |t|
      t.string :description
      t.string :code
      t.timestamps
    end
    add_index :machines, :code, :unique => true
  end
end
