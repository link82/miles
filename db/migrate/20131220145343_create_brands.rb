class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :code
      t.string :description

      t.timestamps
    end
    add_index :brands, :code, :unique => true
  end
end
