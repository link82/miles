class AddI18nToStitchTypes < ActiveRecord::Migration
  def up
    StitchType.create_translation_table! :description => :string, :full_description => :text
  end
  def down
    StitchType.drop_translation_table!
  end
end
