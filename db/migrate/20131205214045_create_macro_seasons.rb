class CreateMacroSeasons < ActiveRecord::Migration
  def change
    create_table :macro_seasons do |t|
      t.string :code
      t.string :description

      t.timestamps
    end
    add_index :macro_seasons, :code, :unique => true
  end
end
