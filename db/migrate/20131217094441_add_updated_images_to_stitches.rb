class AddUpdatedImagesToStitches < ActiveRecord::Migration
  def change
    add_column :stitches, :updated_images, :boolean
    add_index :stitches, :updated_images, :unique => false
  end
end
