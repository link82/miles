class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :description
	  t.string :code
      t.timestamps
    end

    add_index :products, :code, :unique => true
  end
end
