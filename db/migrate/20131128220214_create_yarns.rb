class CreateYarns < ActiveRecord::Migration
  def change
    create_table :yarns do |t|
      t.string :supplier_code
      t.string :supplier_name
      t.string :description
      t.string :code
      t.timestamps
    end

    add_index :yarns, :code, :unique => true
  end
end
