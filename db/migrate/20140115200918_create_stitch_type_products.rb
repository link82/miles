class CreateStitchTypeProducts < ActiveRecord::Migration
  def change
    create_table :stitch_type_products do |t|
      t.string :stitch_type_code
      t.integer :stitch_type_id
      t.string :product_code
      t.integer :product_id
      t.text :description
      t.string :category
      t.integer :year
      t.string :season
      t.string :brand
      t.string :model
      t.boolean :updated_picture
      t.boolean :updated_sketch

      t.timestamps
    end

    add_index :stitch_type_products, :stitch_type_id, :unique => false
    add_index :stitch_type_products, :product_id, :unique => false

  end
end
