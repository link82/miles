class CreateStitchYarns < ActiveRecord::Migration
  def change
    create_table :stitch_yarns do |t|
      t.integer :stitch_id
      t.integer :yarn_id

      t.timestamps
    end
    add_index :stitch_yarns, :stitch_id, :unique => false
    add_index :stitch_yarns, :yarn_id, :unique => false
  end
end
