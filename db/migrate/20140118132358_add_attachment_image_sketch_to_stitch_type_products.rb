class AddAttachmentImageSketchToStitchTypeProducts < ActiveRecord::Migration
  def self.up
    change_table :stitch_type_products do |t|
      t.attachment :image
      t.attachment :sketch
    end
  end

  def self.down
    drop_attached_file :stitch_type_products, :image
    drop_attached_file :stitch_type_products, :sketch
  end
end
