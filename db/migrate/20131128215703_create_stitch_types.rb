class CreateStitchTypes < ActiveRecord::Migration
  def change
    create_table :stitch_types do |t|
      t.integer :parent_id
      t.string :code
      t.boolean :leaf
      t.string :description
      t.text :full_description

      t.timestamps
    end
    add_index :stitch_types, :code, :unique => true
    add_index :stitch_types, :parent_id, :unique => false
  end
end
