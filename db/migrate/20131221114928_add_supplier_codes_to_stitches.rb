class AddSupplierCodesToStitches < ActiveRecord::Migration
  def change
    add_column :stitches, :supplier_codes, :string
  end
end
