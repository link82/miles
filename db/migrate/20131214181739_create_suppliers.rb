class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :code
      t.string :description

      t.timestamps
    end
    add_index :suppliers, :code, :unique => true
  end
end
