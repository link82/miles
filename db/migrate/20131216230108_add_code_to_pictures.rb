class AddCodeToPictures < ActiveRecord::Migration
  def change
    add_column :pictures, :code, :string
    add_index :pictures, :code, :unique => false
  end
end
