class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :description
      t.integer :stitch_id
      t.timestamps
    end

    add_index :pictures, :stitch_id, :unique => false
  end
end
