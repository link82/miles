class AddI18nToStitches < ActiveRecord::Migration
def up
    Stitch.create_translation_table! :description => :text, :composition => :string
  end
  def down
    Stitch.drop_translation_table!
  end
end
