class CreateStitchProducts < ActiveRecord::Migration
  def change
    create_table :stitch_products do |t|
      t.integer :stitch_id
      t.integer :product_id
      t.integer :category_id
      t.string :brand
      t.string :model
      t.string :description
      t.string :code
      t.timestamps
    end

    add_index :stitch_products, :code, :unique => true
    add_index :stitch_products, :stitch_id, :unique => false
    add_index :stitch_products, :product_id, :unique => false
  end
end
