class CreateStitches < ActiveRecord::Migration
  def change
    create_table :stitches do |t|
      t.integer :year
      t.string :season_code
      t.integer :machine_id
      t.string :composition
      t.integer :brand_id
      t.integer :thinness
      t.boolean :stretch
      t.boolean :vanise
      t.string :description
      t.string :code
      t.string :yarn_codes
      t.string :product_codes
      t.string :stitch_type_codes
      t.integer :stitch_type_id
      t.timestamps
    end

    add_index :stitches, :code, :unique => true
    add_index :stitches, :stitch_type_id, :unique => false
  end
end
