#!/bin/bash
#require 'rack-1.0.1'

#Collecting options
while getopts d:t: option
do
  case "${option}"
  in
          c) CLEAN_DB=true;;
  esac
done

if [ -z "$CLEAN_DB" ]; then
  CLEAN_DB=false
fi


if $CLEAN_DB; then
  echo "flushing local db"

  RAILS_ENV=development rake db:drop
  RAILS_ENV=development rake db:create
  RAILS_ENV=development rake db:migrate

fi

echo "Connecting to remote server"
taps pull postgres://deployer:d3pl0y3r@localhost/miles_development http://link:pochacco@miles.bluetouchdev.com:3200